@extends('layouts.back')
@section('content')
    <div class="content-page" @if(!Auth::check()) style="margin-left: 0px !important" @endif>
        <div class="content">
            <div class="container-fluid">
                <div class="row page-title align-items-center">
                    <div class="col-sm-4 col-xl-6">
                        <h4 class="mb-1 mt-0">Dashboard</h4>
                    </div>
                </div>

                <!-- content -->
                <div class="row">
                    <div class="col-md-6 col-xl-3">
                        <a href="{{url('kuesioners')}}">
                            <div class="card">
                                <div class="card-body p-0">
                                    <div class="media p-3">
                                        <div class="media-body">
                                            <span class="text-muted text-uppercase font-size-12 font-weight-bold">Kuesioner</span>
                                            <h2 class="mb-0">{{$kuesioner_count}}</h2>
                                        </div>
                                        <div class="align-self-center">
                                            <i class="icon-xl text-primary" data-feather="file-text"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-md-6 col-xl-3">
                        <a href="{{url('tracers')}}">
                            <div class="card">
                                <div class="card-body p-0">
                                    <div class="media p-3">
                                        <div class="media-body">
                                            <span class="text-muted text-uppercase font-size-12 font-weight-bold">Tracer</span>
                                            <h2 class="mb-0">{{$tracer_count}}</h2>
                                        </div>
                                        <div class="align-self-center">
                                            <i class="icon-xl text-success" data-feather="inbox"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>
        </div> <!-- content -->
    </div>
@endsection