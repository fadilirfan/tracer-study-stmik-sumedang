@extends('layouts.back')
@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#provincies').change(function() {
                var id = $(this).val();
                $.ajax({
                    url: "{{ route('regencies') }}",
                    method: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id": id
                    },
                    async: false,
                    dataType: 'json',
                    success: function(data) {
                        console.log(data);
                        var html = '';
                        var i;
                        for (i = 0; i < data.length; i++) {
                            html += '<option value="' + data[i].id + '">' + data[i].name +
                                '</option>';
                        }
                        $('.regencies').html(html);
                    }
                });
            });
            $('#whatsapp').change(function(){
                var whaval = $('#whatsapp').is(':checked');
                var telpval = $('#telpomsmh').val();
                console.log(telpval);
                if (whaval == true) {
                    $('#whatsappmhs').val(telpval).attr('readonly', 'true');
                } else {
                    $('#whatsappmhs').val('').attr('readonly', 'false');
                }
            })
        });

    </script>
    <script type='text/javascript'>
        function hide1() {
            if (document.getElementById) { // DOM3 = IE5, NS6
                document.getElementById('f9').style.display = 'none';
                document.getElementById('f10').style.display = 'none';
                document.getElementById('f11').style.display = 'table-row';
                //document.getElementById('f12').style.display = 'table-row';
                document.getElementById('f13').style.display = 'table-row';
                document.getElementById('f14').style.display = 'table-row';
                document.getElementById('f15').style.display = 'table-row';
                document.getElementById('f16').style.display = 'table-row';
            } else {
                if (document.layers) { // Netscape 4
                    document.f9.style.display = 'none';
                    document.f10.style.display = 'none';
                    document.f11.style.display = 'table-row';
                    //document.f12.style.display = 'table-row';
                    document.f13.style.display = 'table-row';
                    document.f14.style.display = 'table-row';
                    document.f15.style.display = 'table-row';
                    document.f16.style.display = 'table-row';
                } else { // IE 4
                    document.all.f9.style.display = 'none';
                    document.all.f10.style.display = 'none';
                    document.all.f11.style.display = 'table-row';
                    //document.all.f12.style.display = 'table-row';
                    document.all.f13.style.display = 'table-row';
                    document.all.f14.style.display = 'table-row';
                    document.all.f15.style.display = 'table-row';
                    document.all.f16.style.display = 'table-row';
                }
            }
        }

        function show1() {
            if (document.getElementById) { // DOM3 = IE5, NS6
                document.getElementById('f9').style.display = 'table-row';
                document.getElementById('f10').style.display = 'table-row';
                document.getElementById('f11').style.display = 'table-row';
                //document.getElementById('f12').style.display = 'table-row';
                document.getElementById('f13').style.display = 'table-row';
                document.getElementById('f14').style.display = 'table-row';
                document.getElementById('f15').style.display = 'table-row';
                document.getElementById('f16').style.display = 'table-row';
            } else {
                if (document.layers) { // Netscape 4
                    document.f9.style.display = 'table-row';
                    document.f10.style.display = 'table-row';
                    document.f11.style.display = 'table-row';
                    //document.f12.style.display = 'table-row';
                    document.f13.style.display = 'table-row';
                    document.f14.style.display = 'table-row';
                    document.f15.style.display = 'table-row';
                    document.f16.style.display = 'table-row';
                } else { // IE 4
                    document.all.f9.style.display = 'table-row';
                    document.all.f10.style.display = 'table-row';
                    document.all.f11.style.display = 'table-row';
                    //document.all.f12.style.display = 'table-row';
                    document.all.f13.style.display = 'table-row';
                    document.all.f14.style.display = 'table-row';
                    document.all.f15.style.display = 'table-row';
                    document.all.f16.style.display = 'table-row';
                }
            }
        }

        function hide2() {
            if (document.getElementById) { // DOM3 = IE5, NS6
                document.getElementById('f11').style.display = 'none';
                //document.getElementById('f12').style.display = 'none';
                document.getElementById('f13').style.display = 'none';
                document.getElementById('f14').style.display = 'none';
                document.getElementById('f15').style.display = 'none';
                document.getElementById('f16').style.display = 'none';
            } else {
                if (document.layers) { // Netscape 4
                    document.id.display = 'none';
                    document.f11.style.display = 'none';
                    //document.f12.style.display = 'none';
                    document.f13.style.display = 'none';
                    document.f14.style.display = 'none';
                    document.f15.style.display = 'none';
                    document.f16.style.display = 'none';
                } else { // IE 4
                    document.all.f11.style.display = 'none';
                    //document.all.f12.style.display = 'none';
                    document.all.f13.style.display = 'none';
                    document.all.f14.style.display = 'none';
                    document.all.f15.style.display = 'none';
                    document.all.f16.style.display = 'none';
                }
            }
        }

        function show2() {
            if (document.getElementById) { // DOM3 = IE5, NS6
                document.getElementById('f11').style.display = 'table-row';
                //document.getElementById('f12').style.display = 'table-row';
                document.getElementById('f13').style.display = 'table-row';
                document.getElementById('f14').style.display = 'table-row';
                document.getElementById('f15').style.display = 'table-row';
                document.getElementById('f16').style.display = 'table-row';
            } else {
                if (document.layers) { // Netscape 4
                    document.id.display = 'none';
                    document.f11.style.display = 'table-row';
                    //document.f12.style.display = 'table-row';
                    document.f13.style.display = 'table-row';
                    document.f14.style.display = 'table-row';
                    document.f15.style.display = 'table-row';
                    document.f16.style.display = 'table-row';
                } else { // IE 4
                    document.all.f11.style.display = 'table-row';
                    //document.all.f12.style.display = 'table-row';
                    document.all.f13.style.display = 'table-row';
                    document.all.f14.style.display = 'table-row';
                    document.all.f15.style.display = 'table-row';
                    document.all.f16.style.display = 'table-row';
                }
            }
        }

        function hide3() {
            if (document.getElementById) { // DOM3 = IE5, NS6
                document.getElementById('f4').style.display = 'none';
                document.getElementById('f5').style.display = 'none';
                document.getElementById('f6').style.display = 'none';
                document.getElementById('f7').style.display = 'none';
                document.getElementById('f7a').style.display = 'none';
            } else {
                if (document.layers) { // Netscape 4
                    document.id.display = 'none';
                    document.f4.style.display = 'none';
                    document.f5.style.display = 'none';
                    document.f6.style.display = 'none';
                    document.f7.style.display = 'none';
                    document.f7a.style.display = 'none';
                } else { // IE 4
                    document.all.f4.style.display = 'none';
                    document.all.f5.style.display = 'none';
                    document.all.f6.style.display = 'none';
                    document.all.f7.style.display = 'none';
                    document.all.f7a.style.display = 'none';
                }
            }
        }

        function show3() {
            if (document.getElementById) { // DOM3 = IE5, NS6
                document.getElementById('f4').style.display = 'table-row';
                document.getElementById('f5').style.display = 'table-row';
                document.getElementById('f6').style.display = 'table-row';
                document.getElementById('f7').style.display = 'table-row';
                document.getElementById('f7a').style.display = 'table-row';
            } else {
                if (document.layers) { // Netscape 4
                    document.id.display = 'none';
                    document.f4.style.display = 'table-row';
                    document.f5.style.display = 'table-row';
                    document.f6.style.display = 'table-row';
                    document.f7.style.display = 'table-row';
                    document.f7a.style.display = 'table-row';
                } else { // IE 4
                    document.all.f4.style.display = 'table-row';
                    document.all.f5.style.display = 'table-row';
                    document.all.f6.style.display = 'table-row';
                    document.all.f7.style.display = 'table-row';
                    document.all.f7a.style.display = 'table-row';
                }
            }
        }

    </script>
@endsection
@section('content')
    <div class="content-page" @if(!Auth::check()) style="margin-left: 0px !important" @endif>
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <div class="row page-title">
                    <div class="col-md-12">
                        {{-- <nav aria-label="breadcrumb" class="float-right mt-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('home') }}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="{{ url('kuesioners') }}">Kuesioner</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Tambah</li>
                            </ol>
                        </nav> --}}
                        <h4 class="mb-1 mt-0">Tambah</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                @if (!(Session::get('success')))
                                    <div id="form">
                                        <h4 class="header-title mt-0 mb-1">Tambah Kuesioner</h4>
                                        <form action="{{ route('kuesioners.store') }}" enctype="multipart/form-data" method="post">
                                            @csrf
                                            <table class='table table-responsive table-borderless'>
                                                <tr>
                                                    <td class='i'>Identitas<br /><span class='h'>f1</span></td>
                                                    <td> Nomor Mahasiswa <sup class="text-danger" title="Wajib Diisi">*</sup></td>
                                                    <td>:</td>
                                                    <td><input class="form-control @error('nimhsmsmh') is-invalid @enderror" type='text' placeholder="Masukan nomor (angka) Mahasiswa / NIM" name='nimhsmsmh' value='{{old('nimhsmsmh')}}' size='20'>
                                                        @error('nimhsmsmh')
                                                            <div class="invalid-feedback">
                                                                {{$message}}
                                                            </div>
                                                        @enderror
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class='h'><span class='h'></span></td>
                                                    <td>Kode PT <sup class="text-danger" title="Wajib Diisi">*</sup></td>
                                                    <td>:</td>
                                                    <td><input class="form-control @error('kdptimsmh') is-invalid @enderror" type='text' value='043142' name='kdptimsmh' size='20' readonly>
                                                        @error('kdptimsmh')
                                                            <div class="invalid-feedback">
                                                                {{$message}}
                                                            </div>
                                                        @enderror
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class='h'><span class='h'></span></td>
                                                    <td>Tahun Lulus <sup class="text-danger" title="Wajib Diisi">*</sup></td>
                                                    <td>:</td>
                                                    <td>
                                                        <select name="tahun_lulus" class="form-control @error('tahun_lulus') is-invalid @enderror" id="">
                                                        <?php
                                                        $current_year = date('Y');
                                                        $five_ago = $current_year - 3;
                                                        for ($i=$five_ago; $i <= $current_year; $i++) { ?>
                                                            <option value="{{$i}}" {{(date('Y') == $i) ? 'selected' : null}}>{{$i}}</option>
                                                        <?php } ?>
                                                        </select>
                                                        @error('tahun_lulus')
                                                            <div class="invalid-feedback">
                                                                {{$message}}
                                                            </div>
                                                        @enderror
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class='h'><span class='h'></span></td>
                                                    <td>Kode Prodi <sup class="text-danger" title="Wajib Diisi">*</sup></td>
                                                    <td>:</td>
                                                    <td>
                                                        <select name="kdpstmsmh" class="form-control @error('') is-invalid @enderror" id="">
                                                            <option value="A1" {{(old('kdptimsmh') == 'A1') ? 'selected' : null}}>Manajemen Informatika</option>
                                                            <option value="A2" {{(old('kdptimsmh') == 'A2') ? 'selected' : null}}>Teknik Informatika</option>
                                                            <option value="A3" {{(old('kdptimsmh') == 'A3') ? 'selected' : null}}>Sistem Informasi</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class='h'><span class='h'></span></td>
                                                    <td>Nama <sup class="text-danger" title="Wajib Diisi">*</sup></td>
                                                    <td>:</td>
                                                    <td><input class="form-control @error('nmmhsmsmh') is-invalid @enderror" type='text' placeholder="Masukan nama lengkap" value='{{old('nmmhsmsmh')}}' name='nmmhsmsmh' size='20'>
                                                        @error('nmmhsmsmh')
                                                            <div class="invalid-feedback">
                                                                {{$message}}
                                                            </div>
                                                        @enderror
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class='h'><span class='h'></span></td>
                                                    <td>Nomor Telepon/HP <sup class="text-danger" title="Wajib Diisi">*</sup></td>
                                                    <td>:</td>
                                                    <td><input class="form-control @error('telpomsmh') is-invalid @enderror" type='text' placeholder="Masukan nomor telepon/hp aktif" value='{{old('telpomsmh')}}' name='telpomsmh' id="telpomsmh" size='20'>
                                                        @error('telpomsmh')
                                                            <div class="invalid-feedback">
                                                                {{$message}}
                                                            </div>
                                                        @enderror
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class='h'><span class='h'></span></td>
                                                    <td>Nomor Whatsapp <sup class="text-danger" title="Wajib Diisi">*</sup></td>
                                                    <td>:</td>
                                                    <td><input class="form-control @error('whatsapp') is-invalid @enderror" type='text' placeholder="Masukan nomor whatsapp aktif" value='{{old('whatsapp')}}' name='whatsapp' id="whatsappmhs" size='20'>
                                                        <div class="custom-control custom-switch">
                                                            <input type="checkbox" class="custom-control-input" id="whatsapp" {{(old('whatsapp') != null) ? 'checked' : null}}>
                                                            <label class="custom-control-label" for="whatsapp">Sama dengan no Telepon/HP</label>
                                                        </div>
                                                        @error('whatsapp')
                                                            <div class="invalid-feedback">
                                                                {{$message}}
                                                            </div>
                                                        @enderror
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class='h'><span class='h'></span></td>
                                                    <td>Alamat Email <sup class="text-danger" title="Wajib Diisi">*</sup></td>
                                                    <td>:</td>
                                                    <td>
                                                        <input class="form-control @error('emailmsmh') is-invalid @enderror" type='email' placeholder="Masukan alamat email anda" value='{{old('emailmsmh')}}' name='emailmsmh' size='40'>
                                                        @error('emailmsmh')
                                                            <div class="invalid-feedback">
                                                                {{$message}}
                                                            </div>
                                                        @enderror
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class='h'><span class='h'></span></td>
                                                    <td>NIK <sup class="text-danger" title="Wajib Diisi">*</sup></td>
                                                    <td>:</td>
                                                    <td><input class="form-control @error('nik') is-invalid @enderror" type='text' placeholder="Masukan NIK" value='{{old('nik')}}' name='nik' size='40'>
                                                        @error('nik')
                                                            <div class="invalid-feedback">
                                                                {{$message}}
                                                            </div>
                                                        @enderror
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class='h'><span class='h'></span></td>
                                                    <td>NPWP</td>
                                                    <td>:</td>
                                                    <td><input class="form-control" type='text' placeholder="Masukan NPWP" value='{{old('npwp')}}' name='npwp' size='40'>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan='4'><b>Tracer Study</td>
                                                </tr>
                                                </td>
                                                </tr>
        
        
                                                <tr>
                                                    <td colspan='4'><b>Kuisioner Wajib</td>
                                                </tr>
                                                <tr>
                                                    <td class='h'>f8</td>
                                                    <td valign='top'>Jelaskan status Anda saat ini?
                                                    </td>
                                                    <td valign='top'>:</td>
                                                    <td>
                                                        <input type='radio' value='1' name='f8' valuetable table-striped
                                                            table-responsive='1' onclick='hide1()' {{(old('f8') == '1') ? 'checked' : 'checked'}}  checked> [1] Bekerja (full time/part
                                                        time) <br />
                                                        <input type='radio' value='3' name='f8' valuetable table-striped
                                                            table-responsive='1' onclick='hide1()' {{(old('f8') == '3') ? 'checked' : null}} > [3] Wiraswasta<br />
                                                        <input type='radio' value='4' name='f8' valuetable table-striped
                                                            table-responsive='1' onclick='hide1()' {{(old('f8') == '4') ? 'checked' : null}} > [4] Melanjutkan Pendidikan
                                                        <br />
                                                        <input type='radio' value='5' name='f8' valuetable table-striped
                                                            table-responsive='1' onclick='hide1()' {{(old('f8') == '5') ? 'checked' : null}} > [5] Tidak Kerja tetapi
                                                        sedang mencari kerja <br />
                                                        <input type='radio' name='f8' value='2' onclick='show1()' {{(old('f8') == '2') ? 'checked' : null}} > [2] Belum
                                                        memungkinkan bekerja
                                                    </td>
                                                </tr>
        
                                                <tr id='f5'>
                                                    <td class='h'>f504</td>
                                                    <td valign='top'>Apakah anda telah mendapatkan pekerjaan <= 6 bulan / termasuk
                                                            bekerja sebelum lulus ? </td>
                                                    <td valign='top'>:</td>
                                                    <td>
                                                        <input type='radio' value='1' name='f504' valuetable table-striped
                                                            table-responsive='1' {{(old('f504') == '5') ? 'checked' : 'checked'}}> [1] Ya <span class='hl'>(f5-04)</span><br />
                                                        Dalam berapa bulan anda mendapatkan pekerjaan ? <input class="form-control"
                                                            type='text' name='f502' size='5' placeholder="Masukan jumlah (angka) bulan" value='{{old('f502')}}'><span
                                                            class='hl'>(f5-02)</span><br />
                                                        Berapa rata-rata pendapatan anda per bulan ? (take home pay)?
                                                        <input class="form-control" type='number' name='f505' size='10'
                                                            value='{{old('f505')}}'  placeholder="Masukan jumlah (angka)"><span class='hl'>(f5-05)</span><br />
                                                        <input type='radio' value='2' name='f504' valuetable table-striped
                                                            table-responsive='1' {{(old('f505') == '5') ? 'checked' : null}}> [2] Tidak <span class='hl'>(f5-04)</span><br />
                                                        Dalam berapa bulan anda mendapatkan pekerjaan ? <input class="form-control"
                                                            type='text' name='f506' size='5' value='{{old('f506')}}' placeholder="Masukan jumlah (angka) bulan"><span
                                                            class='hl'>(f5-06)</span><br />
                                                    </td>
                                                </tr>
        
                                                <tr id='f5a'>
                                                    <td class='h'>f510</td>
                                                    <td valign='top'>Dimana lokasi tempat Anda bekerja? ?
                                                    </td>
                                                    <td valign='top'>:</td>
                                                    <td>
                                                        Propinsi : <select class="form-control" name='f5a1' id="provincies">
                                                            <option value=''>Pilih Propinsi</option>
                                                            @foreach ($provincies as $item)
                                                                <option value='{{ $item->id }}'>{{ $item->name }}</option>
                                                            @endforeach
                                                        </select><span class='hl'>(f5a1)</span><br />
                                                        Kab/Kota: <select class="form-control regencies" name='f5a2'>
                                                            <option value=''>Pilih Kabupaten/Kota </option>
                                                        </select><span class='hl'>(f5a2)</span><br />
                                                    </td>
                                                </tr>
        
                                                <tr id='f11'>
                                                    <td class='h'>f11</td>
                                                    <td valign='top'>Apa jenis perusahaan/instansi/institusi tempat anda bekerja
                                                        sekarang?
                                                    </td>
                                                    <td valign='top'>:</td>
                                                    <td>
                                                        <table class='table table-responsive'>
                                                            <tr>
                                                                <td>
                                                                    <input type='radio' name='f1101' value='1' {{ (old('f1101') == '1') ? 'cheked' : 'checked' }} > [1] Instansi
                                                                    pemerintah<br />
                                                                    <input type='radio' name='f1101' value='6' {{ (old('f1101') == '6') ? 'cheked' : null }}> [6]
                                                                    BUMN/BUMD<br />
                                                                    <input type='radio' name='f1101' value='7' {{ (old('f1101') == '7') ? 'cheked' : null }}> [7]
                                                                    Institusi/Organisasi Multilateral<br />
                                                                    <input type='radio' name='f1101' value='2' {{ (old('f1101') == '2') ? 'cheked' : null }}> [2] Organisasi
                                                                    non-profit/Lembaga Swadaya Masyarakat<br />
                                                                    <input type='radio' name='f1101' value='3' {{ (old('f1101') == '3') ? 'cheked' : null }}> [3] Perusahaan
                                                                    swasta<br />
                                                                    <input type='radio' name='f1101' value='4' {{ (old('f1101') == '4') ? 'cheked' : null }}> [4]
                                                                    Wiraswasta/perusahaan sendiri<br />
                                                                    <input type='radio' name='f1101' value='5' {{ (old('f1101') == '5') ? 'cheked' : null }}> [5] Lainnya,
                                                                    tuliskan:
                                                                </td>
                                                                <td><span class='hl'>(f11-01)</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td><input class="form-control" type='text' name='f1102' value='{{old('f1102')}}'
                                                                        size='50'></td>
                                                                <td><span class='hl'>(f11-02)</span></td>
                                                            </tr>
                                                        </table>
                                            </td>
                                            </tr>
        
        
                                            <tr id='f5b'>
                                                <td class='h'>f5b</td>
                                                <td valign='top'>Apa nama perusahaan/kantor tempat Anda bekerja?
                                                </td>
                                                <td valign='top'>:</td>
                                                <td>
                                                    <input class="form-control" type='text' name='f5b' size='50' value='{{old('f5b')}}'  placeholder="Masukan nama perusahaan"><span
                                                        class='hl'>(f5b)</span><br />
                                                </td>
                                            </tr>
        
                                            <tr id='f5c'>
                                                <td class='h'>f5c</td>
                                                <td valign='top'>Bila berwiraswasta, apa posisi/jabatan Anda saat ini ?
                                                </td>
                                                <td valign='top'>:</td>
                                                <td>
                                                    <select class="form-control" name='f5c'>
                                                        <option value=''>Pilih Posisi</option>
                                                        <option value='1' {{(old('f5c') == '1') ? 'selected' : null}} >[1] Founder</option>
                                                        <option value='2' {{(old('f5c') == '2') ? 'selected' : null}} >[2] Co-Founder</option>
                                                        <option value='3' {{(old('f5c') == '3') ? 'selected' : null}} >[3] Staff</option>
                                                        <option value='4' {{(old('f5c') == '4') ? 'selected' : null}} >[4] Freelance/Kerja Lepas</option>
                                                    </select><br />
                                                </td>
                                            </tr>
        
                                            <tr id='f5d'>
                                                <td class='h'>f5d</td>
                                                <td valign='top'>Apa tingkat tempat kerja Anda?
                                                </td>
                                                <td valign='top'>:</td>
                                                <td>
                                                    <select class="form-control" name='f5d'>
                                                        <option value=''>Pilih Tingkatan</option>
                                                        <option value='1' {{(old('f5d') == '1') ? 'selected' : null}}>[1] Lokal/wilayah/wiraswasta tidak berbadan hukum
                                                        </option>
                                                        <option value='2' {{(old('f5d') == '2') ? 'selected' : null}}>[2] Nasional/wiraswasta berbadan hukum</option>
                                                        <option value='3' {{(old('f5d') == '3') ? 'selected' : null}}>[3] Multinasional/internasional</option>
                                                    </select><br />
                                                </td>
                                            </tr>
        
                                            <tr id='f18'>
                                                <td class='h'>f18</td>
                                                <td valign='top'>Pertanyaan studi lanjut
                                                </td>
                                                <td valign='top'>:</td>
                                                <td>
                                                    Sumber Biaya : <select class="form-control" name='f18a'>
                                                        <option value=''>Pilih Sumberbiaya</option>
                                                        <option value='1' {{(old('f18a') == '1') ? 'selected' : null}}>[1] Biaya Sendiri</option>
                                                        <option value='2' {{(old('f18a') == '2') ? 'selected' : null}}>[2] Beasiswa</option>
                                                    </select><span class='hl'>(f18a)</span><br />
                                                    Perguruan Tinggi : 
                                                        <input type="text" class="form-control" name="f18b" value="{{old('f18b')}}" placeholder="Masukan nama perguruan tinggi">
        
                                                        <span class='hl'>(f18b)</span><br />
                                                    Program Studi: <input type="text" class="form-control" name="f18c" value="{{old('f18c')}}" placeholder="Masukan program studi">
                                                    </select><span class='hl'>(f18c)</span><br />
                                                    Tanggal Masuk : <input class="form-control" type='date' name='f18d' size='50'
                                                        value="{{old('f18d')}}"><span class='hl'>(f18d)</span><br />
                                                </td>
                                            </tr>
        
                                            </td>
                                            </tr>
        
        
                                            <tr id='f12'>
                                                <td class='h'>f12</td>
                                                <td valign='top'>Sebutkan sumberdana dalam pembiayaan kuliah?
                                                </td>
                                                <td valign='top'>:</td>
                                                <td>
                                                    <table class='table table-responsive'>
                                                        <tr>
                                                            <td>
                                                                <input type='radio' name='f1201' value='1' {{(old('f1201') == '1') ? 'checked' : 'checked'}} > [1] Biaya
                                                                Sendiri / Keluarga<br />
                                                                <input type='radio' name='f1201' value='2' {{(old('f1201') == '2') ? 'checked' : null}} > [2] Beasiswa
                                                                ADIK<br />
                                                                <input type='radio' name='f1201' value='3' {{(old('f1201') == '3') ? 'checked' : null}} > [3] Beasiswa
                                                                BIDIKMISI<br />
                                                                <input type='radio' name='f1201' value='4' {{(old('f1201') == '4') ? 'checked' : null}} > [4] Beasiswa
                                                                PPA<br />
                                                                <input type='radio' name='f1201' value='5' {{(old('f1201') == '5') ? 'checked' : null}} > [5] Beasiswa
                                                                AFIRMASI<br />
                                                                <input type='radio' name='f1201' value='6' {{(old('f1201') == '6') ? 'checked' : null}} > [6] Beasiswa
                                                                Perusahaan/Swasta<br />
                                                                <input type='radio' name='f1201' value='7' {{(old('f1201') == '7') ? 'checked' : null}} > [7] Lainnya,
                                                                tuliskan:
                                                            </td>
                                                            <td><span class='hl'>(f12-01)</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><input class="form-control" type='text' name='f1202' value='{{old('f1202')}}' size='50'>
                                                            </td>
                                                            <td><span class='hl'>(f12-02)</span></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
        
        
                                            <tr id='f14'>
                                                <td class='h'>f14</td>
                                                <td valign='top'>
                                                    Seberapa erat hubungan antara bidang studi dengan pekerjaan anda?
                                                </td>
                                                <td valign='top'>:</td>
                                                <td>
                                                    <span>
                                                        <input type='radio' name='f14' value='1' {{(old('f14') == '1') ? 'checked' : 'checked'}} > [1] Sangat Erat<br />
                                                        <input type='radio' name='f14' value='2' {{(old('f14') == '2') ? 'checked' : null}} > [2] Erat<br />
                                                        <input type='radio' name='f14' value='3' {{(old('f14') == '3') ? 'checked' : null}} > [3] Cukup Erat<br />
                                                        <input type='radio' name='f14' value='4' {{(old('f14') == '4') ? 'checked' : null}} > [4] Kurang Erat<br />
                                                        <input type='radio' name='f14' value='5' {{(old('f14') == '5') ? 'checked' : null}} > [5] Tidak Sama Sekali
                                                        <br />
                                                    </span>
                                                </td>
                                            </tr>
        
                                            <tr id='f15'>
                                                <td class='h'>f15</td>
                                                <td valign='top'>
                                                    Tingkat pendidikan apa yang paling tepat/sesuai untuk pekerjaan anda
                                                    saat ini?
                                                </td>
                                                <td valign='top'>:</td>
                                                <td>
                                                    <span>
                                                        <input type='radio' name='f15' value='1' {{(old('f15') == '1') ? 'checked' : 'checked'}} > [1] Setingkat Lebih
                                                        Tinggi<br />
                                                        <input type='radio' name='f15' value='2' {{(old('f15') == '2') ? 'checked' : null}} > [2] Tingkat yang
                                                        Sama<br />
                                                        <input type='radio' name='f15' value='3' {{(old('f15') == '3') ? 'checked' : null}} > [3] Setingkat Lebih
                                                        Rendah<br />
                                                        <input type='radio' name='f15' value='4' {{(old('f15') == '4') ? 'checked' : null}} > [4] Tidak Perlu Pendidikan
                                                        Tinggi<br />
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class='h'>f17</td>
                                                <td valign='top'>
                                                    Pada saat lulus, pada tingkat mana kompetensi di bawah ini anda kuasai? (<b>A</b>)
                                                    <br />
                                                    Pada saat ini, pada tingkat mana kompetensi di bawah ini diperlukan dalam pekerjaan?
                                                    (<b>B</b>)
                                                </td>
                                                <td valign='top'>:</td>
                                                <td>
                                                    <table class='table table-striped table-responsive'>
                                                        <tr>
                                                            <th colspan='5' style='align:center;'>A</th>
                                                            <th>&nbsp;
                                                            <th colspan='5'>B</th>
                                                        </tr>
                                                        <tr>
                                                            <th colspan='2'>Sangat Rendah</th>
                                                            <th>&nbsp;</th>
                                                            <th colspan='2'>Sangat Tinggi</th>
                                                            <th>&nbsp;</th>
                                                            <th colspan='2'>Sangat Rendah</th>
                                                            <th>&nbsp;</th>
                                                            <th colspan='2'>Sangat Tinggi</th>
                                                        </tr>
                                                        <tr>
                                                            <th>1</th>
                                                            <th>2</th>
                                                            <th>3</th>
                                                            <th>4</th>
                                                            <th>5</th>
                                                            <th>&nbsp;
                                                            <th>1</th>
                                                            <th>2</th>
                                                            <th>3</th>
                                                            <th>4</th>
                                                            <th>5</th>
                                                        </tr>
        
                                                        <tr>
                                                            <td><input type='radio' name='f1761' value='1' {{(old('f1761') == '1' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1761' value='2' {{(old('f1761') == '2' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1761' value='3' {{(old('f1761') == '3' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1761' value='4' {{(old('f1761') == '4' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1761' value='5' {{(old('f1761') == '5' ) ? 'checked' : null}}></td>
                                                            <td>Etika<span class='hl'>(f1761) (f1762)</span></td>
                                                            <td><input type='radio' name='f1762' value='1' {{(old('f1762') == '1' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1762' value='2' {{(old('f1762') == '2' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1762' value='3' {{(old('f1762') == '3' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1762' value='4' {{(old('f1762') == '4' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1762' value='5' {{(old('f1762') == '5' ) ? 'checked' : null}}></td>
                                                        </tr>
        
                                                        <tr>
                                                            <td><input type='radio' name='f1763' value='1' {{(old('f1763') == '1' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1763' value='2' {{(old('f1763') == '2' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1763' value='3' {{(old('f1763') == '3' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1763' value='4' {{(old('f1763') == '4' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1763' value='5' {{(old('f1763') == '5' ) ? 'checked' : null}}></td>
                                                            <td>Keahlian berdasarkan bidang ilmu<span class='hl'>(f1763) (f1764)</span>
                                                            </td>
                                                            <td><input type='radio' name='f1764' value='1' {{(old('f1764') == '1' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1764' value='2' {{(old('f1764') == '2' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1764' value='3' {{(old('f1764') == '3' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1764' value='4' {{(old('f1764') == '4' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1764' value='5' {{(old('f1764') == '5' ) ? 'checked' : null}}></td>
                                                        </tr>
        
                                                        <tr>
                                                            <td><input type='radio' name='f1765' value='1' {{(old('f1765') == '1' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1765' value='2' {{(old('f1765') == '2' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1765' value='3' {{(old('f1765') == '3' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1765' value='4' {{(old('f1765') == '4' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1765' value='5' {{(old('f1765') == '5' ) ? 'checked' : null}}></td>
                                                            <td>Bahasa Inggris<span class='hl'>(f1765) (f1766)</span></td>
                                                            <td><input type='radio' name='f1766' value='1' {{(old('f1766') == '1' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1766' value='2' {{(old('f1766') == '2' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1766' value='3' {{(old('f1766') == '3' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1766' value='4' {{(old('f1766') == '4' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1766' value='5' {{(old('f1766') == '5' ) ? 'checked' : null}}></td>
                                                        </tr>
        
                                                        <tr>
                                                            <td><input type='radio' name='f1767' value='1' {{(old('f1767') == '1' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1767' value='2' {{(old('f1767') == '2' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1767' value='3' {{(old('f1767') == '3' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1767' value='4' {{(old('f1767') == '4' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1767' value='5' {{(old('f1767') == '5' ) ? 'checked' : null}}></td>
                                                            <td>Penggunaan Teknologi Informasi<span class='hl'>(f1767) (f1768)</span>
                                                            </td>
                                                            <td><input type='radio' name='f1768' value='1' {{(old('f1768') == '1' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1768' value='2' {{(old('f1768') == '2' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1768' value='3' {{(old('f1768') == '3' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1768' value='4' {{(old('f1768') == '4' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1768' value='5' {{(old('f1768') == '5' ) ? 'checked' : null}}></td>
                                                        </tr>
        
                                                        <tr>
                                                            <td><input type='radio' name='f1769' value='1' {{(old('f1769') == '1' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1769' value='2' {{(old('f1769') == '2' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1769' value='3' {{(old('f1769') == '3' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1769' value='4' {{(old('f1769') == '4' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1769' value='5' {{(old('f1769') == '5' ) ? 'checked' : null}}></td>
                                                            <td>Komunikasi<span class='hl'>(f1769) (f1770)</span></td>
                                                            <td><input type='radio' name='f1770' value='1' {{(old('f1770') == '1' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1770' value='2' {{(old('f1770') == '2' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1770' value='3' {{(old('f1770') == '3' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1770' value='4' {{(old('f1770') == '4' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1770' value='5' {{(old('f1770') == '5' ) ? 'checked' : null}}></td>
                                                        </tr>
        
                                                        <tr>
                                                            <td><input type='radio' name='f1771' value='1' {{(old('f1771') == '1' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1771' value='2' {{(old('f1771') == '2' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1771' value='3' {{(old('f1771') == '3' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1771' value='4' {{(old('f1771') == '4' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1771' value='5' {{(old('f1771') == '5' ) ? 'checked' : null}}></td>
                                                            <td>Kerja sama tim<span class='hl'>(f1771) (f1772)</span></td>
                                                            <td><input type='radio' name='f1772' value='1' {{(old('f1772') == '1' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1772' value='2' {{(old('f1772') == '2' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1772' value='3' {{(old('f1772') == '3' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1772' value='4' {{(old('f1772') == '4' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1772' value='5' {{(old('f1772') == '5' ) ? 'checked' : null}}></td>
                                                        </tr>
        
                                                        <tr>
                                                            <td><input type='radio' name='f1773' value='1' {{(old('f1773') == '1' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1773' value='2' {{(old('f1773') == '2' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1773' value='3' {{(old('f1773') == '3' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1773' value='4' {{(old('f1773') == '4' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1773' value='5' {{(old('f1773') == '5' ) ? 'checked' : null}}></td>
                                                            <td>Pengembangan Diri<span class='hl'>(f1773) (f1774)</span></td>
                                                            <td><input type='radio' name='f1774' value='1' {{(old('f1774') == '1' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1774' value='2' {{(old('f1774') == '2' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1774' value='3' {{(old('f1774') == '3' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1774' value='4' {{(old('f1774') == '4' ) ? 'checked' : null}}></td>
                                                            <td><input type='radio' name='f1774' value='5' {{(old('f1774') == '5' ) ? 'checked' : null}}></td>
                                                        </tr>
        
                                                    </table>
                                                </td>
                                            </tr>
        
        
                                            <tr>
                                                <td colspan='4'><b>Kuisioner Opsional</td>
                                            </tr>
                                            <tr id='f2'>
                                                <td class='h'>f2</td>
                                                <td valign='top'>
                                                    Menurut anda seberapa besar penekanan pada metode pembelajaran di bawah ini
                                                    dilaksanakan di program studi anda?
                                                </td>
                                                <td valign='top'>:</td>
                                                <td>
                                                    <table class='table table-striped table-responsive'>
                                                        <tr>
                                                            <td colspan='2'><b>Perkuliahan</b> <span class='hl'>f21</span></td>
                                                </td>
                                            <tr>
                                                <td>
                                                    <input type='radio' name='f21' value='1' {{(old('f21') == '1' ) ? 'checked' : 'checked'}}> [1] Sangat Besar<br />
                                                    <input type='radio' name='f21' value='2' {{(old('f21') == '2' ) ? 'checked' : null}}> [2] Besar<br />
                                                    <input type='radio' name='f21' value='3' {{(old('f21') == '3' ) ? 'checked' : null}}> [3] Cukup Besar<br />
                                                    <input type='radio' name='f21' value='4' {{(old('f21') == '4' ) ? 'checked' : null}}> [4] Kurang<br />
                                                    <input type='radio' name='f21' value='5' {{(old('f21') == '5' ) ? 'checked' : null}}> [5] Tidak Sama Sekali<br />
                                                </td>
                                                <td><span class='hl'>f21</span></td>
                                            </tr>
                                            </table>
                                            <table class='table table-striped table-responsive'>
                                                <tr>
                                                    <td colspan='2'><b>Demonstrasi</b> <span class='hl'>f22</span></td>
                                                    </td>
                                                <tr>
                                                    <td>
                                                        <input type='radio' name='f22' value='1' {{(old('f22') == '1' ) ? 'checked' : 'checked'}}> [1] Sangat Besar<br />
                                                        <input type='radio' name='f22' value='2' {{(old('f22') == '2' ) ? 'selected' : null}}> [2] Besar<br />
                                                        <input type='radio' name='f22' value='3' {{(old('f22') == '3' ) ? 'selected' : null}}> [3] Cukup Besar<br />
                                                        <input type='radio' name='f22' value='4' {{(old('f22') == '4' ) ? 'selected' : null}}> [4] Kurang<br />
                                                        <input type='radio' name='f22' value='5' {{(old('f22') == '5' ) ? 'selected' : null}}> [5] Tidak Sama Sekali<br />
                                                    </td>
                                                    <td><span class='hl'>f22</span></td>
                                                </tr>
                                            </table>
                                            <table class='table table-striped table-responsive'>
                                                <tr>
                                                    <td colspan='2'><b>Partisipasi dalam proyek riset</b> <span class='hl'>f23</span>
                                                    </td>
                                                    </td>
                                                <tr>
                                                    <td>
                                                        <input type='radio' name='f23' value='1' {{(old('f23') == '1' ) ? 'checked' : 'checked'}}> [1] Sangat Besar<br />
                                                        <input type='radio' name='f23' value='2' {{(old('f23') == '2' ) ? 'selected' : null}}> [2] Besar<br />
                                                        <input type='radio' name='f23' value='3' {{(old('f23') == '3' ) ? 'selected' : null}}> [3] Cukup Besar<br />
                                                        <input type='radio' name='f23' value='4' {{(old('f23') == '4' ) ? 'selected' : null}}> [4] Kurang<br />
                                                        <input type='radio' name='f23' value='5' {{(old('f23') == '5' ) ? 'selected' : null}}> [5] Tidak Sama Sekali<br />
                                                    </td>
                                                    <td><span class='hl'>f23</span></td>
                                                </tr>
                                            </table>
                                            <table class='table table-striped table-responsive'>
                                                <tr>
                                                    <td colspan='2'><b>Magang</b> <span class='hl'>f24</span></td>
                                                    </td>
                                                <tr>
                                                    <td>
                                                        <input type='radio' name='f24' value='1' {{(old('f24') == '1' ) ? 'checked' : 'checked'}}> [1] Sangat Besar<br />
                                                        <input type='radio' name='f24' value='2' {{(old('f24') == '2' ) ? 'selected' : null}}> [2] Besar<br />
                                                        <input type='radio' name='f24' value='3' {{(old('f24') == '3' ) ? 'selected' : null}}> [3] Cukup Besar<br />
                                                        <input type='radio' name='f24' value='4' {{(old('f24') == '4' ) ? 'selected' : null}}> [4] Kurang<br />
                                                        <input type='radio' name='f24' value='5' {{(old('f24') == '5' ) ? 'selected' : null}}> [5] Tidak Sama Sekali<br />
                                                    </td>
                                                    <td><span class='hl'>f24</span></td>
                                                </tr>
                                            </table>
                                            <table class='table table-striped table-responsive'>
                                                <tr>
                                                    <td colspan='2'><b>Praktikum</b> <span class='hl'>f25</span></td>
                                                    </td>
                                                <tr>
                                                    <td>
                                                        <input type='radio' name='f25' value='1' {{(old('f25') == '1' ) ? 'checked' : 'checked'}}> [1] Sangat Besar<br />
                                                        <input type='radio' name='f25' value='2' {{(old('f25') == '2' ) ? 'selected' : null}}> [2] Besar<br />
                                                        <input type='radio' name='f25' value='3' {{(old('f25') == '3' ) ? 'selected' : null}}> [3] Cukup Besar<br />
                                                        <input type='radio' name='f25' value='4' {{(old('f25') == '4' ) ? 'selected' : null}}> [4] Kurang<br />
                                                        <input type='radio' name='f25' value='5' {{(old('f25') == '5' ) ? 'selected' : null}}> [5] Tidak Sama Sekali<br />
                                                    </td>
                                                    <td><span class='hl'>f25</span></td>
                                                </tr>
                                            </table>
                                            <table class='table table-striped table-responsive'>
                                                <tr>
                                                    <td colspan='2'><b>Kerja Lapangan</b> <span class='hl'>f26</span></td>
                                                    </td>
                                                <tr>
                                                    <td>
                                                        <input type='radio' name='f26' value='1' {{(old('f26') == '1' ) ? 'checked' : 'checked'}}> [1] Sangat Besar<br />
                                                        <input type='radio' name='f26' value='2' {{(old('f26') == '2' ) ? 'selected' : null}}> [2] Besar<br />
                                                        <input type='radio' name='f26' value='3' {{(old('f26') == '3' ) ? 'selected' : null}}> [3] Cukup Besar<br />
                                                        <input type='radio' name='f26' value='4' {{(old('f26') == '4' ) ? 'selected' : null}}> [4] Kurang<br />
                                                        <input type='radio' name='f26' value='5' {{(old('f26') == '5' ) ? 'selected' : null}}> [5] Tidak Sama Sekali<br />
                                                    </td>
                                                    <td><span class='hl'>f26</span></td>
                                                </tr>
                                            </table>
                                            <table class='table table-striped table-responsive'>
                                                <tr>
                                                    <td colspan='2'><b>Diskusi</b> <span class='hl'>f27</span></td>
                                                    </td>
                                                <tr>
                                                    <td>
                                                        <input type='radio' name='f27' value='1' {{(old('f27') == '1' ) ? 'checked' : 'checked'}}> [1] Sangat Besar<br />
                                                        <input type='radio' name='f27' value='2' {{(old('f27') == '2' ) ? 'selected' : null}}> [2] Besar<br />
                                                        <input type='radio' name='f27' value='3' {{(old('f27') == '3' ) ? 'selected' : null}}> [3] Cukup Besar<br />
                                                        <input type='radio' name='f27' value='4' {{(old('f27') == '4' ) ? 'selected' : null}}> [4] Kurang<br />
                                                        <input type='radio' name='f27' value='5' {{(old('f27') == '5' ) ? 'selected' : null}}> [5] Tidak Sama Sekali<br />
                                                    </td>
                                                    <td><span class='hl'>f27</span></td>
                                                </tr>
                                            </table>
                                            </td>
                                            </tr>
                                            <tr>
                                                <td id='f3' class='h'>f3</td>
                                                <td valign='top'>Kapan anda mulai mencari pekerjaan? <i>Mohon pekerjaan sambilan tidak
                                                        dimasukkan</i></td>
                                                <td valign='top'>:</td>
                                                <td>
                                                    <table class='table table-striped table-responsive'>
                                                        <tr>
                                                            <td><span class='hl'>f301</span>&nbsp;&nbsp;<input id='f301' type='radio'
                                                                    name='f301' value='1' onclick='show3()' {{(old('f301') == '1' ) ? 'checked' : 'checked'}}> [1] Kira-kira <input
                                                                    type='text' class="form-control" name='f302' value='{{old('f302')}}' size='5' placeholder="Masukan (angka) bulan">
                                                                &nbsp;bulan sebelum lulus &nbsp;&nbsp;<span class='hl'>f302</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class='hl'>f301</span>&nbsp;&nbsp;<input id='f301' type='radio'
                                                                    name='f301' value='2' onclick='show3()' {{(old('f301') == '2' ) ? 'selected' : null}}> [2] Kira-kira <input
                                                                    type='text' class="form-control" name='f303' value='{{old('f303')}}' size='5' placeholder="Masukan (angka) bulan">
                                                                &nbsp;bulan sesudah lulus &nbsp;&nbsp;<span class='hl'>f303</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class='hl'>f301</span>&nbsp;&nbsp;<input id='f303' type='radio'
                                                                    name='f301' value='3' onclick='hide3()' {{(old('f301') == '3' ) ? 'selected' : null}}> [3] Saya tidak mencari
                                                                kerja (<i>Langsung ke pertanyaan f8</i>)</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr id='f4'>
                                                <td class='h'>f4</td>
                                                <td valign='top'>Bagaimana anda mencari pekerjaan tersebut?
                                                    <i>Jawaban bisa lebih dari satu</i>
                                                </td>
                                                <td valign='top'>:</td>
                                                <td>
                                                    <table class='table table-striped table-responsive' style="padding:5px;">
                                                        <tr>
                                                            <td>
                                                                <input name='f401' type='checkbox' value='1' {{(old('f401') == '1' ) ? 'checked' : 'checked'}}> [1] Melalui iklan di
                                                                koran/majalah, brosur <span class='hl'>&nbsp;&nbsp;f4-01</span><br />
                                                                <input name='f402' type='checkbox' value='1' {{(old('f402') == '1' ) ? 'checked' : null}}> [1] Melamar ke perusahaan
                                                                tanpa mengetahui lowongan yang ada <span
                                                                    class='hl'>&nbsp;&nbsp;f4-02</span><br />
                                                                <input name='f403' type='checkbox' value='1' {{(old('f403') == '1' ) ? 'checked' : null}}> [1] Pergi ke bursa/pameran
                                                                kerja <span class='hl'>&nbsp;&nbsp;f4-03</span><br />
                                                                <input name='f404' type='checkbox' value='1' {{(old('f404') == '1' ) ? 'checked' : null}}> [1] Mencari lewat
                                                                internet/iklan online/milis <span
                                                                    class='hl'>&nbsp;&nbsp;f4-04</span><br />
                                                                <input name='f405' type='checkbox' value='1' {{(old('f405') == '1' ) ? 'checked' : null}}> [1] Dihubungi oleh
                                                                perusahaan<span class='hl'>&nbsp;&nbsp;f4-05</span><br />
                                                                <input name='f406' type='checkbox' value='1' {{(old('f406') == '1' ) ? 'checked' : null}}> [1] Menghubungi
                                                                Kemenakertrans<span class='hl'>&nbsp;&nbsp;f4-06</span><br />
                                                                <input name='f407' type='checkbox' value='1' {{(old('f407') == '1' ) ? 'checked' : null}}> [1] Menghubungi agen
                                                                tenaga kerja komersial/swasta<span
                                                                    class='hl'>&nbsp;&nbsp;f4-07</span><br />
                                                                <input name='f408' type='checkbox' value='1' {{(old('f408') == '1' ) ? 'checked' : null}}> [1] Memeroleh informasi
                                                                dari pusat/kantor pengembangan karir fakultas/universitas <span
                                                                    class='hl'>&nbsp;&nbsp;f4-08</span><br />
                                                                <input name='f409' type='checkbox' value='1' {{(old('f409') == '1' ) ? 'checked' : null}}> [1] Menghubungi kantor
                                                                kemahasiswaan/hubungan alumni <span
                                                                    class='hl'>&nbsp;&nbsp;f4-09</span><br />
                                                                <input name='f410' type='checkbox' value='1' {{(old('f410') == '1' ) ? 'checked' : null}}> [1] Membangun jejaring
                                                                (<i>network</i>) sejak masih kuliah <span
                                                                    class='hl'>&nbsp;&nbsp;f4-10</span><br />
                                                                <input name='f411' type='checkbox' value='1' {{(old('f411') == '1' ) ? 'checked' : null}}> [1] Melalui relasi
                                                                (misalnya dosen, orang tua, saudara, teman, dll.)<span
                                                                    class='hl'>&nbsp;&nbsp;f4-11</span><br />
                                                                <input name='f412' type='checkbox' value='1' {{(old('f412') == '1' ) ? 'checked' : null}}> [1] Membangun bisnis
                                                                sendiri<span class='hl'>&nbsp;&nbsp;f4-12</span><br />
                                                                <input name='f413' type='checkbox' value='1' {{(old('f413') == '1' ) ? 'checked' : null}}> [1] Melalui penempatan
                                                                kerja atau magang<span class='hl'>&nbsp;&nbsp;f4-13</span><br />
                                                                <input name='f414' type='checkbox' value='1' {{(old('f414') == '1' ) ? 'checked' : null}}> [1] Bekerja di tempat yang
                                                                sama dengan tempat kerja semasa kuliah <span
                                                                    class='hl'>&nbsp;&nbsp;f4-14</span><br />
                                                                <input name='f415' type='checkbox' value='1' {{(old('f415') == '1' ) ? 'checked' : null}}> [1] Lainnya: <span
                                                                    class='hl'>&nbsp;&nbsp;f4-15</span>
                                                            </td>
                                                            <td><span class='h'></span></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <input type='text' class="form-control" size='40' name='f416' value='{{old('f416')}}'>
                                                            </td>
                                                            <td><span class='hl'>&nbsp;&nbsp;f4-16</span></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
        
                                            <!---
                                                            <tr id='f5'><td class='h'>f5</td><td valign='top'>Berapa perusahaan/instansi/institusi yang sudah anda lamar (lewat surat atau e-mail) sebelum anda memeroleh pekerjaan pertama?
                                                            
                                                            </td><td valign='top'>:</td><td><input type='text' class="form-control" name='f500' size='5'> perusahaan/instansi/institusi
                                                            </td></tr>
                                                            
                                                            <tr id='f6'><td class='h'>f6</td><td valign='top'>Berapa bulan waktu yang dihabiskan (sebelum dan sesudah kelulusan) untuk memeroleh pekerjaan pertama?
                                                            </td><td valign='top'>:</td><td>
                                                            <input type='radio' name='f0601' value='1'>
                                                            Kira-kira <input type='text' class="form-control" name='f0602' size='5'> bulan sebelum lulus ujian &nbsp;&nbsp;<span class='hl'>(f6-01, f6-02)</span><br />
                                                            <input type='radio' name='f0603' value='1'>&nbsp;Kira-kira <input type='text' class="form-control" name='f0604' size='5'> bulan setelah lulus ujian &nbsp;&nbsp;<span class='hl'>(f6-03, f6-04)</span><br />
                                                            </td></tr>
                                                            --->
        
                                            <tr id='f6'>
                                                <td class='h'>f6</td>
                                                <td valign='top'>Berapa perusahaan/instansi/institusi yang sudah anda lamar (lewat surat
                                                    atau e-mail) sebelum anda memeroleh pekerjaan pertama?
        
                                                </td>
                                                <td valign='top'>:</td>
                                                <td><input type='text' class="form-control" name='f6' size='5' value='{{old('f6')}}' placeholder="Masukan jumlah (angka)">
                                                    perusahaan/instansi/institusi
                                                </td>
                                            </tr>
        
        
                                            <tr id='f7'>
                                                <td class='h'>f7</td>
                                                <td valign='top'>Berapa banyak perusahaan/instansi/institusi yang merespons lamaran
                                                    anda?
                                                </td>
                                                <td valign='top'>:</td>
                                                <td> <input type='text' class="form-control" name='f7' size='5' value='{{old('f7')}}' placeholder="Masukan jumlah (angka)">
                                                    perusahaan/instansi/institusi
                                                </td>
                                            </tr>
                                            <tr id='f7a'>
                                                <td class='h'>f7a</td>
                                                <td valign='top'>Berapa banyak perusahaan/instansi/institusi yang mengundang anda untuk
                                                    wawancara?
                                                </td>
                                                <td valign='top'>:</td>
                                                <td> <input type='text' class="form-control" name='f7a' size='5' value='{{old('f7a')}}' placeholder="Masukan jumlah (angka)">
                                                    perusahaan/instansi/institusi
                                                </td>
                                            </tr>
                                            <tr id='f10'>
                                                <td class='h'>f10</td>
                                                <td valign='top'>
                                                    Apakah anda aktif mencari pekerjaan dalam 4 minggu terakhir? <i> Pilihlah Satu
                                                        Jawaban. KEMUDIAN LANJUT KE f17 </i>
                                                </td>
                                                <td valign='top'>:</td>
                                                <td>
                                                    <span>
                                                        <table class='table table-striped table-responsive'>
                                                            <tr>
                                                                <td>
                                                                    <input type='radio' name='f1001' value='1' onclick='hide2()' {{(old('f22') == '1' ) ? 'checked' : 'checked'}}> [1]
                                                                    Tidak<br />
                                                                    <input type='radio' name='f1001' value='2' onclick='hide2()' {{(old('f22') == '1' ) ? 'selected' : null}}> [2]
                                                                    Tidak, tapi saya sedang menunggu hasil lamaran kerja<br />
                                                                    <input type='radio' name='f1001' value='3' onclick='hide2()' {{(old('f22') == '1' ) ? 'selected' : null}}> [3]
                                                                    Ya, saya akan mulai bekerja dalam 2 minggu ke depan<br />
                                                                    <input type='radio' name='f1001' value='4' onclick='hide2()' {{(old('f22') == '1' ) ? 'selected' : null}}> [4]
                                                                    Ya, tapi saya belum pasti akan bekerja dalam 2 minggu ke depan<br />
                                                                    <input type='radio' name='f1001' value='5' {{(old('f22') == '5' ) ? 'selected' : null}}> [5] Lainnya<br />
                                                                </td>
                                                                <td><span class='hl'>f10-01</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <input type='text' class="form-control" name='f1002' size='60'
                                                                        maxlength='80' value='{{old('f1002')}}'>
                                                                </td>
                                                                <td><span class='hl'>f10-02</span></td>
                                                            </tr>
                                                        </table>
                                                </td>
                                            </tr>
        
        
                                            <tr id='f16'>
                                                <td class='h'>f16</td>
                                                <td valign='top'>
                                                    Jika menurut anda pekerjaan anda saat ini tidak sesuai dengan pendidikan anda,
                                                    mengapa anda mengambilnya? Jawaban bisa lebih dari satu
                                                </td>
                                                <td valign='top'>:</td>
                                                <td>
                                                    <span>
                                                        <input type='checkbox' name='f1601' value='1' {{(old('f22') == '1' ) ? 'checked' : 'checked'}}> [1] Pertanyaan tidak sesuai;
                                                        pekerjaan saya sekarang sudah sesuai dengan pendidikan saya. <span
                                                            class='hl'>f16-01</span><br />
                                                        <input type='checkbox' name='f1602' value='2' {{(old('f22') == '2' ) ? 'selected' : null}}> [2] Saya belum mendapatkan
                                                        pekerjaan yang lebih sesuai.<span class='hl'>f16-02</span><br />
                                                        <input type='checkbox' name='f1603' value='3' {{(old('f22') == '3' ) ? 'selected' : null}}> [3] Di pekerjaan ini saya
                                                        memeroleh prospek karir yang baik. <span class='hl'>f16-03</span><br />
                                                        <input type='checkbox' name='f1604' value='4' {{(old('f22') == '4' ) ? 'selected' : null}}> [4] Saya lebih suka bekerja di
                                                        area pekerjaan yang tidak ada hubungannya dengan pendidikan saya. <span
                                                            class='hl'>f16-04</span><br />
                                                        <input type='checkbox' name='f1605' value='5' {{(old('f22') == '5' ) ? 'selected' : null}}> [5] Saya dipromosikan ke posisi
                                                        yang kurang berhubungan dengan pendidikan saya dibanding posisi sebelumnya.<span
                                                            class='hl'>f16-05</span><br />
                                                        <input type='checkbox' name='f1606' value='6' {{(old('f22') == '1' ) ? 'selected' : null}}> [6] Saya dapat memeroleh
                                                        pendapatan yang lebih tinggi di pekerjaan ini. <span
                                                            class='hl'>f16-06</span><br />
                                                        <input type='checkbox' name='f1607' value='7' {{(old('f22') == '1' ) ? 'selected' : null}}> [7] Pekerjaan saya saat ini lebih
                                                        aman/terjamin/secure <span class='hl'>f16-07</span><br />
                                                        <input type='checkbox' name='f1608' value='8' {{(old('f22') == '1' ) ? 'selected' : null}}> [8] Pekerjaan saya saat ini lebih
                                                        menarik <span class='hl'>f16-08</span><br />
                                                        <input type='checkbox' name='f1609' value='9' {{(old('f22') == '1' ) ? 'selected' : null}}> [9] Pekerjaan saya saat ini lebih
                                                        memungkinkan saya mengambil pekerjaan tambahan/jadwal yang fleksibel, dll.<span
                                                            class='hl'>f16-09</span><br />
                                                        <input type='checkbox' name='f1610' value='10' {{(old('f22') == '1' ) ? 'selected' : null}}> [10] Pekerjaan saya saat ini
                                                        lokasinya lebih dekat dari rumah saya. <span class='hl'>f16-10</span><br />
                                                        <input type='checkbox' name='f1611' value='11' {{(old('f22') == '1' ) ? 'selected' : null}}> [11] Pekerjaan saya saat ini
                                                        dapat lebih menjamin kebutuhan keluarga saya. <span
                                                            class='hl'>f16-11</span><br />
                                                        <input type='checkbox' name='f1612' value='12' {{(old('f22') == '1' ) ? 'selected' : null}}> [12] Pada awal meniti karir ini,
                                                        saya harus menerima pekerjaan yang tidak berhubungan dengan pendidikan saya.
                                                        <span class='hl'>f16-12</span><br />
                                                        <input type='checkbox' name='f1613' value='13' {{(old('f22') == '1' ) ? 'selected' : null}}> [13] Lainnya: <span
                                                            class='hl'>f16-13</span><br />
                                                        <input type='text' class="form-control" name='f1614' size='60' value='{{old('f1614')}}'
                                                            maxlength='80'><span class='hl'>f16-14</span>
                                                    </span>
                                                </td>
                                            </tr>
                                            </table>
                                            <button class="btn btn-primary" type="submit"><i data-feather="file-plus" class="icon-xs"></i> Submit Kuesioner</button>
                                        </form>
                                    </div>
                                @else
                                    <div class="row align-items-center">
                                        <div class="col-xl-2 col-lg-3 col-6">
                                            <img src="{{asset('assets/back/images/success.png')}}" class="mr-4 align-self-center img-fluid "
                                            alt="cal" />
                                        </div>
                                        <div class="col-xl-10 col-lg-9">
                                            <div class="mt-4 mt-lg-0">
                                                <h5 class="mt-0 mb-1 font-weight-bold text-success">Data berhasil ditambahkan <i data-feather="check-circle"></i> </h5>
                                                <p class="text-muted mb-2">Terimakasih banyak {!! '<b>'.$_GET['nama_lengkap'].'</b>' !!} sudah mengisi formulir kuesioner, informasi selanjutnya akan dihubungi melalui no telp/whatsapp terdaftar. Untuk informasi lainnya terkait tracer study bisa menghubungi kontak whatsapp dibawah.</p>

                                                <a href="https://wa.me/6285220717928" class="btn btn-success mt-2 mr-1" id="btn-new-event"><i
                                                        class="uil-whatsapp"></i> Contact (Irfan Fadil)</a>
                                                <a href="{{url('tracers/kuesioner')}}" class="btn btn-white mt-2"><i class="uil-sync"></i> Isi Kembali ?</a>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div> <!-- end col-->
                </div>

            </div> <!-- container-fluid -->

        </div> <!-- content -->
    @endsection
