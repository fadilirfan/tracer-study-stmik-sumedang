@extends('layouts.back')
@section('js')
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#profile-img").change(function(){
        readURL(this);
    });
</script>
@endsection
@section('content')
<div class="content-page">
    <div class="content">
        
        <!-- Start Content-->
        <div class="container-fluid">
            <div class="row page-title">
                <div class="col-md-12">
                    <nav aria-label="breadcrumb" class="float-right mt-1">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{url('users')}}">User</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Tambah</li>
                        </ol>
                    </nav>
                    <h4 class="mb-1 mt-0">Tambah</h4>
                </div>
            </div>
            
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="header-title mt-0 mb-1">Tambah User</h4>
                            <form class="needs-validation" action="{{route('users.store')}}" method="POST" enctype="multipart/form-data" novalidate>
                                @csrf
                                <div class="form-group mb-3">
                                    <label for="validationCustom01">Nama Lengkap</label>
                                    <input type="text" class="form-control" name="fullname" id="validationCustom01" placeholder="Nama Lengkap" value="{{old('fullname') ?? null}}" required>
                                    @error('fullname')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group mb-3">
                                    <label for="validationCustomUsername">Username</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupPrepend">@</span>
                                        </div>
                                        <input type="text" class="form-control" id="validationCustomUsername" name="username" value="{{old('username') ?? null}}" placeholder="Username" aria-describedby="inputGroupPrepend"
                                            required>
                                        @error('username')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="validationCustom01">Email</label>
                                    <input type="email" class="form-control" name="email" id="validationCustom01" placeholder="Email" value="{{old('email') ?? null}}" required>
                                    @error('email')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group mb-3">
                                    <label for="validationCustom01">Phone Number</label>
                                    <input type="number" class="form-control" name="phone" id="validationCustom01" placeholder="Phone Number" value="{{old('phone') ?? null}}">
                                    @error('phone')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group mb-3">
                                    <label for="validationCustom01">Jenis Kelamin</label>
                                    <div class="custom-control custom-radio mb-2">
                                        <input type="radio" value="Laki-laki" id="gender1" name="gender"
                                            class="custom-control-input">
                                        <label class="custom-control-label" for="gender1">Laki - laki</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" value="Perempuan" id="gender2" name="gender"
                                            class="custom-control-input">
                                        <label class="custom-control-label" for="gender2">Perempuan</label>
                                    </div>
                                    @error('gender')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group mb-3">
                                    <label for="validationCustom01">Tanggal Lahir</label>
                                    <input type="date" class="form-control" name="birthday" id="validationCustom01" placeholder="Tanggal Lahir" value="{{old('birthday') ?? null}}">
                                    @error('birthday')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group mb-3">
                                    <label for="validationCustom01">User Level</label>
                                    <select name="id_user_level" class="custom-select mb-2">
                                        @foreach ($user_levels as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('id_user_level')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group mb-3">
                                    <label>Password</label>
                                    <div>
                                        <input type="password" class="form-control" name="password" id="validationCustom01" placeholder="Password" value="{{old('password') ?? null}}">
                                        @error('password')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="mt-2">
                                        <input type="password" class="form-control" name="confirm_password" id="validationCustom01" placeholder="Re-type Password" value="{{old('confirm_password') ?? null}}">
                                        @error('confirm_password')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label class="mb-2">Profile Picture</label>
                                    <br>
                                    <img src="" id="profile-img-tag" width="200px" style="border-radius: 50%;object-fit:cover;" />
                                    <input type="file" class="form-control" name="picture" id="profile-img">
                                    @error('picture')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <button class="btn btn-primary" type="submit"> <i data-feather="user-plus" class="icon-xs"></i> Tambah User</button>
                            </form>

                        </div> <!-- end card-body-->
                    </div> <!-- end card-->
                </div> <!-- end col-->
            </div>

        </div> <!-- container-fluid -->

    </div> <!-- content -->
@endsection