@extends('layouts.back')
@section('css')
    <link href="{{asset('assets/back/libs/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/back/libs/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/back/libs/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/back/libs/datatables/select.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" /> 
@endsection
@section('js')
    <!-- datatable js -->
    <script src="{{asset('assets/back/libs/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/back/libs/datatables/dataTables.bootstrap4.min.j')}}s"></script>
    <script src="{{asset('assets/back/libs/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/back/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
    
    <script src="{{asset('assets/back/libs/datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/back/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/back/libs/datatables/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/back/libs/datatables/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/back/libs/datatables/buttons.print.min.js')}}"></script>

    <script src="{{asset('assets/back/libs/datatables/dataTables.keyTable.min.js')}}"></script>
    <script src="{{asset('assets/back/libs/datatables/dataTables.select.min.js')}}"></script>

    <!-- Datatables init -->
    <script src="{{asset('assets/back/js/pages/datatables.init.js')}}"></script>
@endsection
@section('content')
<div class="content-page">
    <div class="content">
        
        <!-- Start Content-->
        <div class="container-fluid">
            <div class="row page-title">
                <div class="col-md-12">
                    <nav aria-label="breadcrumb" class="float-right mt-1">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active" aria-current="page">User</li>
                        </ol>
                    </nav>
                    <h4 class="mb-1 mt-0">User</h4>
                </div>
            </div>
            

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4 class="header-title mt-0 mb-1">Data User</h4>
                                </div>
                                <div class="col-md-6 text-right">
                                    <a href="{{route('users.create')}}" class="btn btn-primary btn-xs" style="margin-bottom: 10px;">Tambah User</a>
                                </div>
                            </div>
                            @if ($msg = Session::get('success'))
                            @endif
                            @if ($msg = Session::get('error'))
                                <div class="alert alert-danger">
                                    {{$msg}}
                                </div>
                            @endif
                            <p class="sub-header text-right">
                            </p>


                            <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Name</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>User Level</th>
                                        @if (Auth::user()->user_level->name == 'Administrator')
                                            <th>Aksi</th>
                                        @endif
                                    </tr>
                                </thead>
                            
                            
                                <tbody>
                                    @foreach ($users as $i)
                                        <tr>
                                            @php
                                                $path = 'input/users/profile/default.png';
                                                if (!empty($i->picture)) {
                                                    $path = Storage::url($i->picture);
                                                }
                                            @endphp
                                            <td><img src="{{$path}}" class="avatar-sm rounded-circle mr-2" alt="Shreyu" /></td>
                                            <td>{{$i->fullname}}</td>
                                            <td>{{$i->username}}</td>
                                            <td>{{$i->email}}</td>
                                            <td>
                                                @if ($i->user_level->name == 'Administrator')
                                                    <span class="badge badge-success">{{$i->user_level->name}}</span>
                                                @else
                                                    <span class="badge badge-secondary">{{$i->user_level->name}}</span>
                                                @endif
                                            </td>
                                            @if (Auth::user()->user_level->name == 'Administrator')
                                            <td>
                                                <a href="{{ route('users.edit', $i->id) }}" class="btn btn-rounded btn-info btn-xs"><i class="anticon anticon-edit"></i> Edit</a>
                                                <form action="{{ route('users.destroy', $i->id) }}" method="post"
                                                    onsubmit="return confirm('Yakin hapus data ini?')">
                                                    @csrf
                                                    @method('delete')
                                                    <button type="submit" class="btn btn-rounded btn-danger btn-xs" style="margin: 5px auto;">
                                                        <i class="anticon anticon-delete"></i> Hapus</button>
                                                </form>
                                            </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            
                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
            </div>
            <!-- end row-->

        </div> <!-- container-fluid -->

    </div> <!-- content -->

@endsection