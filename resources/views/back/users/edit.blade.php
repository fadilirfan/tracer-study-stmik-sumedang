@extends('layouts.back')
@section('js')
<script type="text/javascript">
    function readURL(input) {
        $('#profile-img-tag-current').hide();
        $('#profile-img-tag').show();
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#profile-img").change(function(){
        readURL(this);
    });
</script>
@if (Session::get('success') || Session::get('error'))
<script>
    $('#ubahPassword').modal('show');
</script>
@endif
@error('new_password')
<script>
    $('#ubahPassword').modal('show');
</script>
@enderror
@error('confirm_password')
<script>
    $('#ubahPassword').modal('show');
</script>
@enderror
@endsection
@section('content')
<div class="content-page">
    <div class="content">
        
        <!-- Start Content-->
        <div class="container-fluid">
            <div class="row page-title">
                <div class="col-md-12">
                    <nav aria-label="breadcrumb" class="float-right mt-1">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{url('users')}}">User</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Edit</li>
                        </ol>
                    </nav>
                    <h4 class="mb-1 mt-0">Edit</h4>
                </div>
            </div>
            
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="header-title mt-0 mb-1">Edit User</h4>
                            <div class="text-right">
                                <button type="button" class="btn btn-outline-warning btn-xs " data-toggle="modal" data-target="#ubahPassword">
                                    <i class="anticon anticon-key"></i> Ubah password ?
                                </button>
                            </div>
                            <form class="needs-validation" action="{{route('users.update', $detail->id)}}" method="POST" enctype="multipart/form-data" novalidate>
                                @csrf
                                @method('PUT')
                                <div class="form-group mb-3">
                                    <label for="">Nama Lengkap</label>
                                    <input type="text" class="form-control @error('fullname') is-invalid @enderror" name="fullname" id="" placeholder="Nama Lengkap" value="{{ $detail->fullname ?? old('fullname') }}" required>
                                    @error('fullname')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group mb-3">
                                    <label for="">Username</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupPrepend">@</span>
                                        </div>
                                        <input type="text" class="form-control @error('username') is-invalid @enderror" id="" name="username" value="{{ $detail->username ?? old('username') }}" placeholder="Username" aria-describedby="inputGroupPrepend"
                                            required>
                                        @error('username')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="">Email</label>
                                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="" placeholder="Email" value="{{ $detail->email ?? old('email') }}" required>
                                    @error('email')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group mb-3">
                                    <label for="">Phone Number</label>
                                    <input type="number" class="form-control @error('phone') is-invalid @enderror" name="phone" id="" placeholder="Phone Number" value="{{ $detail->phone ?? old('phone') }}">
                                    @error('phone')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group mb-3">
                                    <label for="">Jenis Kelamin</label>
                                    <div class="custom-control custom-radio mb-2">
                                        <input type="radio" value="Laki-laki" id="gender1" name="gender"
                                            class="custom-control-input" {{($detail->gender == 'Laki-laki') ? 'checked' : null}}>
                                        <label class="custom-control-label" for="gender1">Laki - laki</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" value="Perempuan" id="gender2" name="gender"
                                            class="custom-control-input" {{($detail->gender == 'Perempuan') ? 'checked' : null}}>
                                        <label class="custom-control-label" for="gender2">Perempuan</label>
                                    </div>
                                    @error('gender')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group mb-3">
                                    <label for="">Tanggal Lahir</label>
                                    <input type="date" class="form-control @error('username') is-invalid @enderror" name="birthday" id="" placeholder="Tanggal Lahir" value="{{ $detail->birthday ?? old('birthday') }}">
                                    @error('birthday')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group mb-3">
                                    <label for="">User Level</label>
                                    <select name="id_user_level" class="custom-select mb-2">
                                        @foreach ($user_levels as $item)
                                            <option value="{{$item->id}}" {{($detail->id_user_level == $item->id) ? 'selected' : null}}>{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('id_user_level')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group mb-3">
                                    <label class="mb-2">Profile Picture</label>
                                    <br>
                                    @if ($detail->picture)
                                    <img src="{{Storage::url($detail->picture)}}" id="profile-img-tag-current"  width="200px" style="border-radius: 50%;object-fit:cover;" /><br>
                                    @endif
                                    <img src="" id="profile-img-tag" width="200px" style="border-radius: 50%;object-fit:cover;" />
                                    <input type="file" class="form-control @error('username') is-invalid @enderror" name="picture" id="profile-img">
                                    @error('picture')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <button class="btn btn-success" type="submit"> <i class="icon-xs" data-feather="save"></i> Simpan Perubahan</button>
                            </form>

                        </div> <!-- end card-body-->
                    </div> <!-- end card-->
                </div> <!-- end col-->
            </div>

        </div> <!-- container-fluid -->

    </div> <!-- content -->
    <div class="modal fade" id="ubahPassword">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ubahPasswordTitle">Ubah Password</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <i class="anticon anticon-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                        @if ($msg = Session::get('success'))
                                    <div class="alert alert-success">
                                        {{$msg}}
                                    </div>
                            @endif
                            @if ($msg = Session::get('error'))
                                    <div class="alert alert-danger">
                                        {{$msg}}
                                    </div>
                            @endif
                    <form action="{{route('change_password', $detail->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="col-md-12">
                        <label for="">Password Lama</label>
                        <input type="password" class="form-control m-b-15 @error('old_password') is-invalid @enderror" name="old_password"  placeholder="Password Lama">
                        @error('old_password')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        <label for="">Password Baru</label>
                        <input type="password" class="form-control m-b-15 @error('new_password') is-invalid @enderror" name="new_password"  placeholder="Password Baru">
                        @error('new_password')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        <label for="">Konfirmasi Password Baru</label>
                        <input type="password" class="form-control m-b-15 @error('confirm_password') is-invalid @enderror" name="confirm_password"  placeholder="Konfirmasi Password Baru">
                        @error('confirm_password')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan perubahan</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection