@extends('layouts.back')
@section('css')
    <link href="{{asset('assets/back/libs/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/back/libs/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/back/libs/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/back/libs/datatables/select.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" /> 
@endsection
@section('js')
    <!-- datatable js -->
    <script src="{{asset('assets/back/libs/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/back/libs/datatables/dataTables.bootstrap4.min.j')}}s"></script>
    <script src="{{asset('assets/back/libs/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/back/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
    
    <script src="{{asset('assets/back/libs/datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/back/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/back/libs/datatables/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/back/libs/datatables/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/back/libs/datatables/buttons.print.min.js')}}"></script>

    <script src="{{asset('assets/back/libs/datatables/dataTables.keyTable.min.js')}}"></script>
    <script src="{{asset('assets/back/libs/datatables/dataTables.select.min.js')}}"></script>

    <script>
        $(document).ready(function() {
            $('#example').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            } );
        } );
    </script>

    <!-- Datatables init -->
    <script src="{{asset('assets/back/js/pages/datatables.init.js')}}"></script>
@endsection
@section('content')
<div class="content-page">
    <div class="content">
        
        <!-- Start Content-->
        <div class="container-fluid">
            <div class="row page-title">
                <div class="col-md-12">
                    <nav aria-label="breadcrumb" class="float-right mt-1">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Tracer</li>
                        </ol>
                    </nav>
                    <h4 class="mb-1 mt-0">Tracer</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4 class="header-title mt-0 mb-1">Data Tracer</h4>
                                </div>
                                <div class="col-md-6 text-right">
                                    <a href="{{route('tracers.create')}}" class="btn btn-primary btn-xs" style="margin-bottom: 10px;">Tambah Tracer</a>
                                </div>
                            </div>
                            @if ($msg = Session::get('success'))
                                <div class="alert alert-success">
                                    {{$msg}}
                                </div>
                            @endif
                            @if ($msg = Session::get('error'))
                                <div class="alert alert-danger">
                                    {{$msg}}
                                </div>
                            @endif
                            <p class="sub-header text-right">
                            </p>


                            <table id="example" class="table table-striped dt-responsive nowrap">
                                <thead>
                                    <tr>
                                        <th>Kode Perguruan Tinggi Mahasiswa</th>
                                        <th>Kode PST</th>
                                        <th>NIM Mahasiswa</th>
                                        <th>Nama Mahasiswa</th>
                                        <th>Telepon Mahasiswa</th>
                                        <th>Email Mahasiswa</th>
                                        <th>Tahun Lulus</th>
                                        <th>f501</th>
                                        <th>f502</th>
                                        <th>f503</th>
                                        <th>f1201</th>
                                        <th>f1202</th>
                                        <th>f8</th>
                                        <th>f14</th>
                                        <th>f15</th>
                                        <th>f1301</th>
                                        <th>f1302</th>
                                        <th>f1303</th>
                                        <th>f21</th>
                                        <th>f22</th>
                                        <th>f23</th>
                                        <th>f24</th>
                                        <th>f25</th>
                                        <th>f26</th>
                                        <th>f27</th>
                                        <th>f301</th>
                                        <th>f302</th>
                                        <th>f303</th>
                                        <th>f401</th>
                                        <th>f402</th>
                                        <th>f403</th>
                                        <th>f404</th>
                                        <th>f405</th>
                                        <th>f406</th>
                                        <th>f407</th>
                                        <th>f408</th>
                                        <th>f409</th>
                                        <th>f4010</th>
                                        <th>f4011</th>
                                        <th>f4012</th>
                                        <th>f4013</th>
                                        <th>f4014</th>
                                        <th>f4015</th>
                                        <th>f4016</th>
                                        <th>f6</th>
                                        <th>f7</th>
                                        <th>f7a</th>
                                        <th>f901</th>
                                        <th>f902</th>
                                        <th>f903</th>
                                        <th>f904</th>
                                        <th>f905</th>
                                        <th>f906</th>
                                        <th>f1001</th>
                                        <th>f1002</th>
                                        <th>f1101</th>
                                        <th>f1102</th>
                                        <th>f1601</th>
                                        <th>f1602</th>
                                        <th>f1603</th>
                                        <th>f1604</th>
                                        <th>f1605</th>
                                        <th>f1606</th>
                                        <th>f1607</th>
                                        <th>f1608</th>
                                        <th>f1609</th>
                                        <th>f16010</th>
                                        <th>f16011</th>
                                        <th>f16012</th>
                                        <th>f16013</th>
                                        <th>f16014</th>
                                        <th>f1701</th>
                                        <th>f1702b</th>
                                        <th>f1703</th>
                                        <th>f1704b</th>
                                        <th>f1705</th>
                                        <th>f1706b</th>
                                        <th>f1707</th>
                                        <th>f1708b</th>
                                        <th>f1709</th>
                                        <th>f1710b</th>
                                        <th>f1710b</th>
                                        <th>f1711</th>
                                        <th>f1712b</th>
                                        <th>f1713</th>
                                        <th>f1714b</th>
                                        <th>f1715</th>
                                        <th>f1716b</th>
                                        <th>f1717</th>
                                        <th>f1718b</th>
                                        <th>f1719</th>
                                        <th>f1720b</th>
                                        <th>f1721</th>
                                        <th>f1722b</th>
                                        <th>f1723</th>
                                        <th>f1724b</th>
                                        <th>f1725</th>
                                        <th>f1726b</th>
                                        <th>f1727</th>
                                        <th>f1728b</th>
                                        <th>f1729</th>
                                        <th>f1730b</th>
                                        <th>f1731</th>
                                        <th>f1732b</th>
                                        <th>f1733</th>
                                        <th>f1734b</th>
                                        <th>f1735</th>
                                        <th>f1736b</th>
                                        <th>f1737</th>
                                        <th>f1738b</th>
                                        <th>f1739</th>
                                        <th>f1740b</th>
                                        <th>f1741</th>
                                        <th>f1742b</th>
                                        <th>f1743</th>
                                        <th>f1744b</th>
                                        <th>f1745</th>
                                        <th>f1746b</th>
                                        <th>f1747</th>
                                        <th>f1748b</th>
                                        <th>f1749</th>
                                        <th>f1750b</th>
                                        <th>f1751</th>
                                        <th>f1752b</th>
                                        <th>f1753</th>
                                        <th>f1754b</th>
                                        @if (Auth::user()->user_level->name == 'Administrator')
                                            <th>Aksi</th>
                                        @endif
                                    </tr>
                                </thead>
                            
                            
                                <tbody>
                                    @foreach ($tracers as $i)
                                        <tr>
                                            <td>{{$i->kdptimsmh}}</td>
                                            <td>{{$i->kdpstmsmh}}</td>
                                            <td>{{$i->nimhsmsmh}}</td>
                                            <td>{{$i->nmmhsmsmh}}</td>
                                            <td>{{$i->telpomsmh}}</td>
                                            <td>{{$i->emailmsmh}}</td>
                                            <td>{{$i->tahun_lulus}}</td>
                                            <td>{{$i->f501}}</td>
                                            <td>{{$i->f502}}</td>
                                            <td>{{$i->f503}}</td>
                                            <td>{{$i->f1201}}</td>
                                            <td>{{$i->f1202}}</td>
                                            <td>{{$i->f8}}</td>
                                            <td>{{$i->f14}}</td>
                                            <td>{{$i->f15}}</td>
                                            <td>{{$i->f1301}}</td>
                                            <td>{{$i->f1302}}</td>
                                            <td>{{$i->f1303}}</td>
                                            <td>{{$i->f21}}</td>
                                            <td>{{$i->f22}}</td>
                                            <td>{{$i->f23}}</td>
                                            <td>{{$i->f24}}</td>
                                            <td>{{$i->f25}}</td>
                                            <td>{{$i->f26}}</td>
                                            <td>{{$i->f27}}</td>
                                            <td>{{$i->f301}}</td>
                                            <td>{{$i->f302}}</td>
                                            <td>{{$i->f303}}</td>
                                            <td>{{$i->f401}}</td>
                                            <td>{{$i->f402}}</td>
                                            <td>{{$i->f403}}</td>
                                            <td>{{$i->f404}}</td>
                                            <td>{{$i->f405}}</td>
                                            <td>{{$i->f406}}</td>
                                            <td>{{$i->f407}}</td>
                                            <td>{{$i->f408}}</td>
                                            <td>{{$i->f409}}</td>
                                            <td>{{$i->f4010}}</td>
                                            <td>{{$i->f4011}}</td>
                                            <td>{{$i->f4012}}</td>
                                            <td>{{$i->f4013}}</td>
                                            <td>{{$i->f4014}}</td>
                                            <td>{{$i->f4015}}</td>
                                            <td>{{$i->f4016}}</td>
                                            <td>{{$i->f6}}</td>
                                            <td>{{$i->f7}}</td>
                                            <td>{{$i->f7a}}</td>
                                            <td>{{$i->f901}}</td>
                                            <td>{{$i->f902}}</td>
                                            <td>{{$i->f903}}</td>
                                            <td>{{$i->f904}}</td>
                                            <td>{{$i->f905}}</td>
                                            <td>{{$i->f906}}</td>
                                            <td>{{$i->f1001}}</td>
                                            <td>{{$i->f1002}}</td>
                                            <td>{{$i->f1101}}</td>
                                            <td>{{$i->f1102}}</td>
                                            <td>{{$i->f1601}}</td>
                                            <td>{{$i->f1602}}</td>
                                            <td>{{$i->f1603}}</td>
                                            <td>{{$i->f1604}}</td>
                                            <td>{{$i->f1605}}</td>
                                            <td>{{$i->f1606}}</td>
                                            <td>{{$i->f1607}}</td>
                                            <td>{{$i->f1608}}</td>
                                            <td>{{$i->f1609}}</td>
                                            <td>{{$i->f16010}}</td>
                                            <td>{{$i->f16011}}</td>
                                            <td>{{$i->f16012}}</td>
                                            <td>{{$i->f16013}}</td>
                                            <td>{{$i->f16014}}</td>
                                            <td>{{$i->f1701}}</td>
                                            <td>{{$i->f1702b}}</td>
                                            <td>{{$i->f1703}}</td>
                                            <td>{{$i->f1704b}}</td>
                                            <td>{{$i->f1705}}</td>
                                            <td>{{$i->f1706b}}</td>
                                            <td>{{$i->f1707}}</td>
                                            <td>{{$i->f1708b}}</td>
                                            <td>{{$i->f1709}}</td>
                                            <td>{{$i->f1710b}}</td>
                                            <td>{{$i->f1710b}}</td>
                                            <td>{{$i->f1711}}</td>
                                            <td>{{$i->f1712b}}</td>
                                            <td>{{$i->f1713}}</td>
                                            <td>{{$i->f1714b}}</td>
                                            <td>{{$i->f1715}}</td>
                                            <td>{{$i->f1716b}}</td>
                                            <td>{{$i->f1717}}</td>
                                            <td>{{$i->f1718b}}</td>
                                            <td>{{$i->f1719}}</td>
                                            <td>{{$i->f1720b}}</td>
                                            <td>{{$i->f1721}}</td>
                                            <td>{{$i->f1722b}}</td>
                                            <td>{{$i->f1723}}</td>
                                            <td>{{$i->f1724b}}</td>
                                            <td>{{$i->f1725}}</td>
                                            <td>{{$i->f1726b}}</td>
                                            <td>{{$i->f1727}}</td>
                                            <td>{{$i->f1728b}}</td>
                                            <td>{{$i->f1729}}</td>
                                            <td>{{$i->f1730b}}</td>
                                            <td>{{$i->f1731}}</td>
                                            <td>{{$i->f1732b}}</td>
                                            <td>{{$i->f1733}}</td>
                                            <td>{{$i->f1734b}}</td>
                                            <td>{{$i->f1735}}</td>
                                            <td>{{$i->f1736b}}</td>
                                            <td>{{$i->f1737}}</td>
                                            <td>{{$i->f1738b}}</td>
                                            <td>{{$i->f1739}}</td>
                                            <td>{{$i->f1740b}}</td>
                                            <td>{{$i->f1741}}</td>
                                            <td>{{$i->f1742b}}</td>
                                            <td>{{$i->f1743}}</td>
                                            <td>{{$i->f1744b}}</td>
                                            <td>{{$i->f1745}}</td>
                                            <td>{{$i->f1746b}}</td>
                                            <td>{{$i->f1747}}</td>
                                            <td>{{$i->f1748b}}</td>
                                            <td>{{$i->f1749}}</td>
                                            <td>{{$i->f1750b}}</td>
                                            <td>{{$i->f1751}}</td>
                                            <td>{{$i->f1752b}}</td>
                                            <td>{{$i->f1753}}</td>
                                            <td>{{$i->f1754b}}</td>
                                            @if (Auth::user()->user_level->name == 'Administrator')
                                            <td>
                                                <a href="{{ route('tracers.edit', $i->id) }}" class="btn btn-rounded btn-info btn-xs"><i class="anticon anticon-edit"></i> Edit</a>
                                                <form action="{{ route('tracers.destroy', $i->id) }}" method="post"
                                                    onsubmit="return confirm('Yakin hapus data ini?')">
                                                    @csrf
                                                    @method('delete')
                                                    <button type="submit" class="btn btn-rounded btn-danger btn-xs" style="margin: 5px auto;">
                                                        <i class="anticon anticon-delete"></i> Hapus</button>
                                                </form>
                                            </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            
                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
            </div>
            <!-- end row-->

        </div> <!-- container-fluid -->

    </div> <!-- content -->

@endsection