@extends('layouts.back')
@section('js')
<script type="text/javascript">
    $(document).ready(function() {
        $('#whatsapp').change(function(){
            var whaval = $('#whatsapp').is(':checked');
            var telpval = $('#telpomsmh').val();
            console.log(telpval);
            if (whaval == true) {
                $('#whatsappmhs').val(telpval).attr('readonly', 'true');
            } else {
                $('#whatsappmhs').val('').attr('readonly', 'false');
            }
        })
    });

</script>
@endsection
@section('content')
    <div class="content-page" @if(!Auth::check()) style="margin-left: 0px !important" @endif>
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <div class="row page-title">
                    <div class="col-md-12">
                        {{-- <nav aria-label="breadcrumb" class="float-right mt-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('home') }}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="{{ url('tracers') }}">Tracer</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Tambah</li>
                            </ol>
                        </nav> --}}
                        <h4 class="mb-1 mt-0">Tambah</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                @if (!(Session::get('success')))
                                    <div id="form">
                                        <h4 class="header-title mt-0 mb-1">Tambah Tracer</h4>
                                        <form action="{{route('tracers.store')}}"
                                            enctype="multipart/form-data" method="post">
                                            @csrf
                                            <table class="table table-responsive table-borderless">
                                                <tbody>
                                                    <tr>
                                                        <td rowspan="8"><strong>Identifikasi</strong><br>f1</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Nomor Mahasiswa <sup class="text-danger" title="Wajib Diisi">*</sup> </td>
                                                        <td>:</td>
                                                        <td>
                                                            <input class="form-control @error('nimhsmsmh') is-invalid @enderror" name="nimhsmsmh" type="text" value="{{old('nimhsmsmh')}}" placeholder="Masukan nomor (angka) Mahasiswa / NIM"/>
                                                            @error('nimhsmsmh')
                                                                <div class="invalid-feedback">
                                                                    {{$message}}
                                                                </div>
                                                            @enderror
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kode PT <sup class="text-danger" title="Wajib Diisi">*</sup></td>
                                                        <td>:</td>
                                                        <td><input class="form-control @error('kdptimsmh') is-invalid @enderror" type='text' value='043142' name='kdptimsmh' size='20' readonly>
                                                            @error('kdptimsmh')
                                                                <div class="invalid-feedback">
                                                                    {{$message}}
                                                                </div>
                                                            @enderror
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tahun Lulus <sup class="text-danger" title="Wajib Diisi">*</sup></td>
                                                        <td>:</td>
                                                        <td>
                                                            <select name="tahun_lulus" class="form-control @error('tahun_lulus') is-invalid @enderror" id="">
                                                            <?php
                                                            $current_year = date('Y');
                                                            $five_ago = $current_year - 3;
                                                            for ($i=$five_ago; $i <= $current_year; $i++) { ?>
                                                                <option value="{{$i}}" {{(date('Y') == $i) ? 'selected' : null}}>{{$i}}</option>
                                                            <?php } ?>
                                                            </select>
                                                            @error('tahun_lulus')
                                                                <div class="invalid-feedback">
                                                                    {{$message}}
                                                                </div>
                                                            @enderror
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kode Prodi <sup class="text-danger" title="Wajib Diisi">*</sup></td>
                                                        <td>:</td>
                                                        <td>
                                                            <select name="kdpstmsmh" class="form-control @error('') is-invalid @enderror" id="">
                                                                <option value="A1" {{(old('kdpstmsmh') == 'A1') ? 'selected' : null}}>Manajemen Informatika</option>
                                                                <option value="A2" {{(old('kdpstmsmh') == 'A2') ? 'selected' : null}}>Teknik Informatika</option>
                                                                <option value="A3" {{(old('kdpstmsmh') == 'A3') ? 'selected' : null}}>Sistem Informasi</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Nama <sup class="text-danger" title="Wajib Diisi">*</sup></td>
                                                        <td>:</td>
                                                        <td><input class="form-control @error('nmmhsmsmh') is-invalid @enderror" placeholder="Masukan nama lengkap" name="nmmhsmsmh" type="text" value="{{old('nmmhsmsmh')}}" />
                                                            @error('nmmhsmsmh')
                                                                <div class="invalid-feedback">
                                                                    {{$message}}
                                                                </div>
                                                            @enderror
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Nomor Telepon/HP <sup class="text-danger" title="Wajib Diisi">*</sup></td>
                                                        <td>:</td>
                                                        <td><input class="form-control @error('nmmhsmsmh') is-invalid @enderror"  placeholder="Masukan nomor telepon/hp aktif" type='text' value='{{old('nmmhsmsmh')}}' name='telpomsmh' id="telpomsmh" size='20'>
                                                            @error('nmmhsmsmh')
                                                                <div class="invalid-feedback">
                                                                    {{$message}}
                                                                </div>
                                                            @enderror
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Nomor Whatsapp <sup class="text-danger" title="Wajib Diisi">*</sup></td>
                                                        <td>:</td>
                                                        <td><input class="form-control @error('whatsapp') is-invalid @enderror" placeholder="Masukan nomor whatsapp aktif" type='text' value='{{old('whatsapp')}}' name='whatsapp' id="whatsappmhs" size='20'>
                                                            <div class="custom-control custom-switch">
                                                                <input type="checkbox" class="custom-control-input" id="whatsapp" {{old('whatsapp') ?? 'checked'}}>
                                                                <label class="custom-control-label" for="whatsapp">Sama dengan no Telepon/HP</label>
                                                            </div>
                                                            @error('whatsapp')
                                                                <div class="invalid-feedback">
                                                                    {{$message}}
                                                                </div>
                                                            @enderror
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>Alamat Email <sup class="text-danger" title="Wajib Diisi">*</sup></td>
                                                        <td>:</td>
                                                        <td><input class="form-control @error('emailmsmh') is-invalid @enderror" placeholder="Masukan alamat email anda" name="emailmsmh" size="40" type="text" value="{{old('emailmsmh')}}" />
                                                            @error('emailmsmh')
                                                                <div class="invalid-feedback">
                                                                    {{$message}}
                                                                </div>
                                                            @enderror
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4"><strong>Tracer Study</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4"><strong>Kuisioner Wajib</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>f5</td>
                                                        <td>Berapa bulan waktu yang dihabiskan (sebelum dan sesudah kelulusan) untuk
                                                            memeroleh pekerjaan pertama?</td>
                                                        <td>:</td>
                                                        <td><input name="f501" type="radio" value="1" {{(old('f501') == '1') ? 'checked' : 'checked'}}/> [1] Kira-kira <input class="form-control"
                                                                name="f502" size="5" type="text" value="{{old('f502')}}" placeholder="Masukan (angka) bulan" /> bulan sebelum lulus
                                                            ujian (f5-01, f5-02)<br /> <input name="f501" type="radio"
                                                                value="2" />&nbsp;[2] Kira-kira <input class="form-control" name="f503" size="5"
                                                                type="text" value="{{old('f503')}}" placeholder="Masukan (angka) bulan" /> bulan setelah lulus ujian (f5-01, f5-03)
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>f12</td>
                                                        <td>Sebutkan sumberdana dalam pembiayaan kuliah?</td>
                                                        <td>:</td>
                                                        <td>
                                                            <table>
                                                                <tbody>
                                                                    <tr>
                                                                        <td><input name="f1201" type="radio" value="1" {{(old('f1201') == '1') ? 'checked' : 'checked'}}/> [1] Biaya
                                                                            Sendiri / Keluarga<br /> <input name="f1201"
                                                                                type="radio" value="2" {{(old('f1201') == '2') ? 'checked' : null}}/> [2] Beasiswa ADIK<br />
                                                                            <input name="f1201" type="radio" value="3" {{(old('f1201') == '3') ? 'checked' : null}}/> [3]
                                                                            Beasiswa BIDIKMISI<br /> <input name="f1201"
                                                                                type="radio" value="4" {{(old('f1201') == '4') ? 'checked' : null}}/> [4] Beasiswa PPA<br />
                                                                            <input name="f1201" type="radio" value="5" {{(old('f1201') == '5') ? 'checked' : null}}/> [5]
                                                                            Beasiswa AFIRMASI<br /> <input name="f1201" type="radio"
                                                                                value="6" {{(old('f1201') == '6') ? 'checked' : null}} /> [6] Beasiswa Perusahaan/Swasta<br />
                                                                            <input name="f1201" type="radio" value="7" {{(old('f1201') == '1') ? 'checked' : null}}/> [7]
                                                                            Lainnya, tuliskan:</td>
                                                                        <td>(f12-01)</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input class="form-control" name="f1202" size="50" type="text" value="{{old('f1202')}}" />
                                                                        </td>
                                                                        <td>(f12-02)</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>f8</td>
                                                        <td>Apakah anda bekerja saat ini (termasuk kerja sambilan dan wirausaha)?
                                                        </td>
                                                        <td>:</td>
                                                        <td><input name="f8" type="radio" value="1" {{(old('f8') == '1') ? 'checked' : 'checked'}}/> [1] Ya <br /> <input
                                                                name="f8" type="radio" value="2" {{(old('f8') == '2') ? 'checked' : null}}/> [2] Tidak</td>
                                                    </tr>
                                                    <tr>
                                                        <td>f14</td>
                                                        <td>Seberapa erat hubungan antara bidang studi dengan pekerjaan anda?</td>
                                                        <td>:</td>
                                                        <td><input name="f14" type="radio" value="1" {{(old('f14') == '1') ? 'checked' : 'checked'}}/> [1] Sangat Erat<br /> <input
                                                                name="f14" type="radio" value="2" {{(old('f14') == '2') ? 'checked' : null}}/> [2] Erat<br /> <input
                                                                name="f14" type="radio" value="3" {{(old('f14') == '3') ? 'checked' : null}}/> [3] Cukup Erat<br /> <input
                                                                name="f14" type="radio" value="4" {{(old('f14') == '4') ? 'checked' : null}}/> [4] Kurang Erat<br /> <input
                                                                name="f14" type="radio" value="5" {{(old('f14') == '5') ? 'checked' : null}}/> [5] Tidak Sama Sekali </td>
                                                    </tr>
                                                    <tr>
                                                        <td>f15</td>
                                                        <td>Tingkat pendidikan apa yang paling tepat/sesuai untuk pekerjaan anda
                                                            saat ini?</td>
                                                        <td>:</td>
                                                        <td><input name="f15" type="radio" value="1" {{(old('f15') == '1') ? 'checked' : 'checked'}}/> [1] Setingkat Lebih
                                                            Tinggi<br /> <input name="f15" type="radio" value="2" {{(old('f15') == '2') ? 'checked' : null}}/> [2] Tingkat
                                                            yang Sama<br /> <input name="f15" type="radio" value="3" {{(old('f15') == '3') ? 'checked' : null}}/> [3]
                                                            Setingkat Lebih Rendah<br /> <input name="f15" type="radio" value="4" {{(old('f15') == '4') ? 'checked' : null}}/>
                                                            [4] Tidak Perlu Pendidikan Tinggi</td>
                                                    </tr>
                                                    <tr>
                                                        <td>f13</td>
                                                        <td>Kira-kira berapa pendapatan anda setiap bulannya?</td>
                                                        <td>:</td>
                                                        <td>
                                                            <table>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Dari Pekerjaan Utama</td>
                                                                        <td>Rp. <input class="form-control" name="f1301" size="20" type="text"
                                                                                value="0" />(f13-01) (Isilah dengan ANGKA saja,
                                                                            tanpa tanda Titik atau Koma)</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Dari Lembur dan Tips</td>
                                                                        <td>Rp. <input class="form-control" name="f1302" size="20" type="text"
                                                                                value="0" />(f13-02) (Isilah dengan ANGKA saja,
                                                                            tanpa tanda Titik atau Koma)</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Dari Pekerjaan Lainnya</td>
                                                                        <td>Rp. <input class="form-control" name="f1303" size="20" type="text"
                                                                                value="0" />(f13-03) (Isilah dengan ANGKA saja,
                                                                            tanpa tanda Titik atau Koma)</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4"><strong>Kuisioner Opsional</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>f2</td>
                                                        <td>Menurut anda seberapa besar penekanan pada metode pembelajaran di bawah
                                                            ini dilaksanakan di program studi anda?</td>
                                                        <td>:</td>
                                                        <td>
                                                            <table>
                                                                <tbody>
                                                                    <tr>
                                                                        <td colspan="2"><strong>Perkuliahan</strong> f21</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input  name="f21" type="radio" value="1" {{(old('f21') == '1') ? 'checked' : 'checked'}}/> [1] Sangat
                                                                            Besar<br /> <input name="f21" type="radio" value="2" {{(old('f21') == '2') ? 'checked' : null}}/>
                                                                            [2] Besar<br /> <input name="f21" type="radio"
                                                                                value="3" {{(old('f21') == '3') ? 'checked' : null}} /> [3] Cukup Besar<br /> <input name="f21"
                                                                                type="radio" value="4" {{(old('f21') == '4') ? 'checked' : null}}/> [4] Kurang<br /> <input 
                                                                                name="f21" type="radio" value="5" {{(old('f21') == '5') ? 'checked' : null}}/> [5] Tidak Sama
                                                                            Sekali</td>
                                                                        <td>f21</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <table>
                                                                <tbody>
                                                                    <tr>
                                                                        <td colspan="2"><strong>Demonstrasi</strong> f22</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f22" type="radio" value="1" {{(old('f22') == '1') ? 'checked' : 'checked'}}/> [1] Sangat
                                                                            Besar<br /> <input name="f22" type="radio" value="2" {{(old('f22') == '2') ? 'checked' : null}}/>
                                                                            [2] Besar<br /> <input name="f22" type="radio"
                                                                                value="3" {{(old('f22') == '3') ? 'selected' : null}} /> [3] Cukup Besar<br /> <input name="f22"
                                                                                type="radio" value="4" {{(old('f22') == '4') ? 'checked' : null}}/> [4] Kurang<br /> <input
                                                                                name="f22" type="radio" value="5" {{(old('f22') == '5') ? 'checked' : null}}/> [5] Tidak Sama
                                                                            Sekali</td>
                                                                        <td>f22</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <table>
                                                                <tbody>
                                                                    <tr>
                                                                        <td colspan="2"><strong>Partisipasi dalam proyek
                                                                                riset</strong> f23</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f23" type="radio" value="1" {{(old('f23') == '1') ? 'checked' : 'checked'}}/> [1] Sangat
                                                                            Besar<br /> <input name="f23" type="radio" value="2" {{(old('f23') == '2') ? 'checked' : null}}/>
                                                                            [2] Besar<br /> <input name="f23" type="radio"
                                                                                value="3" {{(old('f23') == '3') ? 'selected' : null}} /> [3] Cukup Besar<br /> <input name="f23"
                                                                                type="radio" value="4" {{(old('f23') == '4') ? 'checked' : null}}/> [4] Kurang<br /> <input
                                                                                name="f23" type="radio" value="5" {{(old('f23') == '5') ? 'checked' : null}}/> [5] Tidak Sama
                                                                            Sekali</td>
                                                                        <td>f23</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <table>
                                                                <tbody>
                                                                    <tr>
                                                                        <td colspan="2"><strong>Magang</strong> f24</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f24" type="radio" value="1" {{(old('f24') == '1') ? 'checked' : 'checked'}}/> [1] Sangat
                                                                            Besar<br /> <input name="f24" type="radio" value="2" {{(old('f24') == '2') ? 'checked' : null}}/>
                                                                            [2] Besar<br /> <input name="f24" type="radio"
                                                                                value="3" {{(old('f24') == '3') ? 'selected' : null}} /> [3] Cukup Besar<br /> <input name="f24"
                                                                                type="radio" value="4" {{(old('f24') == '4') ? 'checked' : null}}/> [4] Kurang<br /> <input
                                                                                name="f24" type="radio" value="5" {{(old('f24') == '5') ? 'checked' : null}}/> [5] Tidak Sama
                                                                            Sekali</td>
                                                                        <td>f24</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <table>
                                                                <tbody>
                                                                    <tr>
                                                                        <td colspan="2"><strong>Praktikum</strong> f25</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f25" type="radio" value="1" {{(old('f25') == '1') ? 'checked' : 'checked'}}/> [1] Sangat
                                                                            Besar<br /> <input name="f25" type="radio" value="2" {{(old('f25') == '2') ? 'checked' : null}}/>
                                                                            [2] Besar<br /> <input name="f25" type="radio"
                                                                                value="3" {{(old('f25') == '3') ? 'selected' : null}} /> [3] Cukup Besar<br /> <input name="f25"
                                                                                type="radio" value="4" {{(old('f25') == '4') ? 'checked' : null}}/> [4] Kurang<br /> <input
                                                                                name="f25" type="radio" value="5" {{(old('f25') == '5') ? 'checked' : null}}/> [5] Tidak Sama
                                                                            Sekali</td>
                                                                        <td>f25</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <table>
                                                                <tbody>
                                                                    <tr>
                                                                        <td colspan="2"><strong>Kerja Lapangan</strong> f26</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f26" type="radio" value="1" {{(old('f26') == '1') ? 'checked' : 'checked'}}/> [1] Sangat
                                                                            Besar<br /> <input name="f26" type="radio" value="2" {{(old('f26') == '2') ? 'checked' : null}}/>
                                                                            [2] Besar<br /> <input name="f26" type="radio"
                                                                                value="3" {{(old('f26') == '3') ? 'selected' : null}} /> [3] Cukup Besar<br /> <input name="f26"
                                                                                type="radio" value="4" {{(old('f26') == '4') ? 'checked' : null}}/> [4] Kurang<br /> <input
                                                                                name="f26" type="radio" value="5" {{(old('f26') == '5') ? 'checked' : null}}/> [5] Tidak Sama
                                                                            Sekali</td>
                                                                        <td>f26</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <table>
                                                                <tbody>
                                                                    <tr>
                                                                        <td colspan="2"><strong>Diskusi</strong> f27</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f27" type="radio" value="1" {{(old('f27') == '1') ? 'checked' : 'checked'}}/> [1] Sangat
                                                                            Besar<br /> <input name="f27" type="radio" value="2" {{(old('f27') == '2') ? 'checked' : null}}/>
                                                                            [2] Besar<br /> <input name="f27" type="radio"
                                                                                value="3" {{(old('f27') == '3') ? 'selected' : null}} /> [3] Cukup Besar<br /> <input name="f27"
                                                                                type="radio" value="4" {{(old('f27') == '4') ? 'checked' : null}}/> [4] Kurang<br /> <input
                                                                                name="f27" type="radio" value="5" {{(old('f27') == '5') ? 'checked' : null}}/> [5] Tidak Sama
                                                                            Sekali</td>
                                                                        <td>f27</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>f3</td>
                                                        <td>Kapan anda mulai mencari pekerjaan? <em>Mohon pekerjaan sambilan tidak
                                                                dimasukkan</em></td>
                                                        <td>:</td>
                                                        <td>
                                                            <table>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>f301 <input name="f301" type="radio" value="1" {{(old('f301') == '1') ? 'checked' : 'checked'}}/> [1]
                                                                            Kira-kira <input class="form-control" name="f302" size="5" type="text"
                                                                                value="{{old('f906')}}" placeholder="Masukan (angka) bulan" /> bulan sebelum lulus f302</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>f301 <input  name="f301" type="radio" value="2" {{(old('f301') == '2') ? 'checked' : null}}/> [2]
                                                                            Kira-kira <input class="form-control" name="f303" size="5" type="text"
                                                                                value="{{old('f906')}}" placeholder="Masukan (angka) bulan" /> bulan sesudah lulus f303</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>f301 <input name="f301" type="radio" value="3" {{(old('f301') == '3') ? 'checked' : null}}/> [3]
                                                                            Saya tidak mencari kerja (<em>Langsung ke pertanyaan
                                                                                f8</em>)</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>f4</td>
                                                        <td>Bagaimana anda mencari pekerjaan tersebut? <em>Jawaban bisa lebih dari
                                                                satu</em></td>
                                                        <td>:</td>
                                                        <td>
                                                            <table>
                                                                <tbody>
                                                                    <tr>
                                                                        <td><input name="f401" type="checkbox" value="1" {{(old('f401') == '1') ? 'checked' : null}} /> [1]
                                                                            Melalui iklan di koran/majalah, brosur f4-01<br />
                                                                            <input name="f402" type="checkbox" value="1" {{(old('f402') == '1') ? 'checked' : null}} /> [1]
                                                                            Melamar ke perusahaan tanpa mengetahui lowongan yang ada
                                                                            f4-02<br /> <input name="f403" type="checkbox"
                                                                                value="1" {{(old('f403') == '1') ? 'checked' : null}} /> [1] Pergi ke bursa/pameran kerja
                                                                            f4-03<br /> <input name="f404" type="checkbox"
                                                                                value="1" {{(old('f404') == '1') ? 'checked' : null}} /> [1] Mencari lewat internet/iklan
                                                                            online/milis f4-04<br /> <input name="f405"
                                                                                type="checkbox" value="1" {{(old('f405') == '1') ? 'checked' : null}} /> [1] Dihubungi oleh
                                                                            perusahaan f4-05<br /> <input name="f406"
                                                                                type="checkbox" value="1" {{(old('f406') == '1') ? 'checked' : null}} /> [1] Menghubungi
                                                                            Kemenakertrans f4-06<br /> <input name="f407"
                                                                                type="checkbox" value="1" {{(old('f407') == '1') ? 'checked' : null}} /> [1] Menghubungi agen
                                                                            tenaga kerja komersial/swasta f4-07<br /> <input
                                                                                name="f408" type="checkbox" value="1" {{(old('f408') == '1') ? 'checked' : null}} /> [1]
                                                                            Memeroleh informasi dari pusat/kantor pengembangan karir
                                                                            fakultas/universitas f4-08<br /> <input name="f409"
                                                                                type="checkbox" value="1" {{(old('f409') == '1') ? 'checked' : null}} /> [1] Menghubungi kantor
                                                                            kemahasiswaan/hubungan alumni f4-09<br /> <input
                                                                                name="f410" type="checkbox" value="1" {{(old('f410') == '1') ? 'checked' : null}} /> [1]
                                                                            Membangun jejaring (<em>network</em>) sejak masih kuliah
                                                                            f4-10<br /> <input name="f411" type="checkbox"
                                                                                value="1" {{(old('f411') == '1') ? 'checked' : null}} /> [1] Melalui relasi (misalnya dosen,
                                                                            orang tua, saudara, teman, dll.) f4-11<br /> <input
                                                                                name="f412" type="checkbox" value="1" {{(old('f412') == '1') ? 'checked' : null}} /> [1]
                                                                            Membangun bisnis sendiri f4-12<br /> <input name="f413"
                                                                                type="checkbox" value="1" {{(old('f413') == '1') ? 'checked' : null}} /> [1] Melalui penempatan
                                                                            kerja atau magang f4-13<br /> <input name="f414"
                                                                                type="checkbox" value="1" {{(old('f414') == '1') ? 'checked' : null}} /> [1] Bekerja di tempat
                                                                            yang sama dengan tempat kerja semasa kuliah f4-14<br />
                                                                            <input name="f415" type="checkbox" value="1" {{(old('f415') == '1') ? 'checked' : null}} /> [1]
                                                                            Lainnya: f4-15</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input class="form-control" name="f416" size="40" type="text" value="{{old('f416')}}" />
                                                                        </td>
                                                                        <td> f4-16</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>f6</td>
                                                        <td>Berapa perusahaan/instansi/institusi yang sudah anda lamar (lewat surat
                                                            atau e-mail) sebelum anda memeroleh pekerjaan pertama?</td>
                                                        <td>:</td>
                                                        <td><input class="form-control" placeholder="Masukan jumlah (angka)" name="f6" size="5" type="text" value="{{old('f6')}}" />
                                                            perusahaan/instansi/institusi</td>
                                                    </tr>
                                                    <tr>
                                                        <td>f7</td>
                                                        <td>Berapa banyak perusahaan/instansi/institusi yang merespons lamaran anda?
                                                        </td>
                                                        <td>:</td>
                                                        <td><input class="form-control" placeholder="Masukan jumlah (angka)" name="f7" size="5" type="text" value="{{old('f7')}}" />
                                                            perusahaan/instansi/institusi</td>
                                                    </tr>
                                                    <tr>
                                                        <td>f7a</td>
                                                        <td>Berapa banyak perusahaan/instansi/institusi yang mengundang anda untuk
                                                            wawancara?</td>
                                                        <td>:</td>
                                                        <td><input class="form-control" placeholder="Masukan jumlah (angka)" name="f7a" size="5" type="text" value="{{old('f7a')}}" />
                                                            perusahaan/instansi/institusi</td>
                                                    </tr>
                                                    <tr>
                                                        <td>f9</td>
                                                        <td>Bagaimana anda menggambarkan situasi anda saat ini? <em> Jawaban bisa
                                                                lebih dari satu</em></td>
                                                        <td>:</td>
                                                        <td>
                                                            <table>
                                                                <tbody>
                                                                    <tr>
                                                                        <td><input name="f901" type="checkbox" value="1" {{(old('f901') == '1') ? 'checked' : null}} /> [1] Saya
                                                                            masih belajar/melanjutkan kuliah profesi atau
                                                                            pascasarjana f9-01<br /> <input name="f902"
                                                                                type="checkbox" value="2" {{(old('f902') == '2') ? 'checked' : null}} /> [2] Saya menikah
                                                                            f9-02<br /> <input name="f903" type="checkbox"
                                                                                value="3" {{(old('f903') == '3') ? 'selected' : null}} /> [3] Saya sibuk dengan keluarga dan
                                                                            anak-anak f9-03<br /> <input name="f904" type="checkbox"
                                                                                value="4" {{(old('f904') == '4') ? 'checked' : null}} /> [4] Saya sekarang sedang mencari
                                                                            pekerjaan f9-04<br /> <input name="f905" type="checkbox"
                                                                                value="5" {{(old('f905') == '5') ? 'checked' : null}} /> [5] Lainnya f9-05</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input class="form-control" maxlength="80" name="f906" size="60" type="text"
                                                                                value="{{old('f906')}}" /> f9-06</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>f10</td>
                                                        <td>Apakah anda aktif mencari pekerjaan dalam 4 minggu terakhir? <em>
                                                                Pilihlah Satu Jawaban. KEMUDIAN LANJUT KE f17 </em></td>
                                                        <td>:</td>
                                                        <td>
                                                            <table>
                                                                <tbody>
                                                                    <tr>
                                                                        <td><input name="f1001" type="radio" value="1" {{(old('f1001') == '1') ? 'checked' : 'checked'}}/> [1]
                                                                            Tidak<br /> <input name="f1001" type="radio"
                                                                                value="2" {{(old('f1001') == '2') ? 'checked' : null}} /> [2] Tidak, tapi saya sedang menunggu
                                                                            hasil lamaran kerja<br /> <input name="f1001"
                                                                                type="radio" value="3" {{(old('f1001') == '3') ? 'checked' : null}}/> [3] Ya, saya akan mulai
                                                                            bekerja dalam 2 minggu ke depan<br /> <input
                                                                                name="f1001" type="radio" value="4" {{(old('f1001') == '4') ? 'checked' : null}}/> [4] Ya, tapi
                                                                            saya belum pasti akan bekerja dalam 2 minggu ke
                                                                            depan<br /> <input name="f1001" type="radio"
                                                                                value="5" {{(old('f1001') == '5') ? 'checked' : null}} /> [5] Lainnya</td>
                                                                        <td>f10-01</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input class="form-control" maxlength="80" name="f1002" size="60" type="text"
                                                                                value="" /></td>
                                                                        <td>f10-02</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>f11</td>
                                                        <td>Apa jenis perusahaan/instansi/institusi tempat anda bekerja sekarang?
                                                        </td>
                                                        <td>:</td>
                                                        <td>
                                                            <table>
                                                                <tbody>
                                                                    <tr>
                                                                        <td><input name="f1101" type="radio" value="1" {{(old('f1101') == '1') ? 'checked' : 'checked'}}/> [1]
                                                                            Instansi pemerintah (termasuk BUMN)<br /> <input
                                                                                name="f1101" type="radio" value="2" {{(old('f1101') == '2') ? 'checked' : null}}/> [2]
                                                                            Organisasi non-profit/Lembaga Swadaya Masyarakat<br />
                                                                            <input name="f1101" type="radio" value="3" {{(old('f1101') == '3') ? 'checked' : null}}/> [3]
                                                                            Perusahaan swasta<br /> <input name="f1101" type="radio"
                                                                                value="4" {{(old('f1101') == '4') ? 'checked' : null}} /> [4] Wiraswasta/perusahaan sendiri<br />
                                                                            <input name="f1101" type="radio" value="5" {{(old('f1101') == '5') ? 'checked' : null}}/> [5]
                                                                            Lainnya, tuliskan:</td>
                                                                        <td>(f11-01)</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input class="form-control" name="f1102" size="50" type="text" value="{{old('f1102')}}" />
                                                                        </td>
                                                                        <td>(f11-02)</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>f16</td>
                                                        <td>Jika menurut anda pekerjaan anda saat ini tidak sesuai dengan pendidikan
                                                            anda, mengapa anda mengambilnya? Jawaban bisa lebih dari satu</td>
                                                        <td>:</td>
                                                        <td><input name="f1601" type="checkbox" value="1" {{(old('f1601') == '1') ? 'checked' : null}} /> [1] Pertanyaan tidak
                                                            sesuai; pekerjaan saya sekarang sudah sesuai dengan pendidikan saya.
                                                            f16-01<br /> <input name="f1602" type="checkbox" value="2" {{(old('f1602') == '2') ? 'checked' : null}} /> [2] Saya
                                                            belum mendapatkan pekerjaan yang lebih sesuai.f16-02<br /> <input
                                                                name="f1603" type="checkbox" value="3" {{(old('f1603') == '3') ? 'selected' : null}} /> [3] Di pekerjaan ini saya
                                                            memeroleh prospek karir yang baik. f16-03<br /> <input name="f1604"
                                                                type="checkbox" value="4" {{(old('f1604') == '4') ? 'checked' : null}} /> [4] Saya lebih suka bekerja di area
                                                            pekerjaan yang tidak ada hubungannya dengan pendidikan saya.
                                                            f16-04<br /> <input name="f1605" type="checkbox" value="5" {{(old('f1605') == '5') ? 'checked' : null}} /> [5] Saya
                                                            dipromosikan ke posisi yang kurang berhubungan dengan pendidikan saya
                                                            dibanding posisi sebelumnya.f16-05<br /> <input name="f1606"
                                                                type="checkbox" value="6" {{(old('f1606') == '4') ? 'checked' : null}} /> [6] Saya dapat memeroleh pendapatan
                                                            yang lebih tinggi di pekerjaan ini. f16-06<br /> <input name="f1607"
                                                                type="checkbox" value="7" {{(old('f1607') == '4') ? 'checked' : null}} /> [7] Pekerjaan saya saat ini lebih
                                                            aman/terjamin/secure f16-07<br /> <input name="f1608" type="checkbox"
                                                                value="8" {{(old('f1608') == '4') ? 'checked' : null}} /> [8] Pekerjaan saya saat ini lebih menarik f16-08<br />
                                                            <input name="f1609" type="checkbox" value="9" {{(old('f1609') == '4') ? 'checked' : null}} /> [9] Pekerjaan saya saat
                                                            ini lebih memungkinkan saya mengambil pekerjaan tambahan/jadwal yang
                                                            fleksibel, dll.f16-09<br /> <input name="f1610" type="checkbox"
                                                                value="10" {{(old('f1610') == '4') ? 'checked' : null}} /> [10] Pekerjaan saya saat ini lokasinya lebih dekat
                                                            dari rumah saya. f16-10<br /> <input name="f1611" type="checkbox"
                                                                value="11" {{(old('f1611') == '4') ? 'checked' : null}} /> [11] Pekerjaan saya saat ini dapat lebih menjamin
                                                            kebutuhan keluarga saya. f16-11<br /> <input name="f1612"
                                                                type="checkbox" value="12" {{(old('f1612') == '4') ? 'checked' : null}} /> [12] Pada awal meniti karir ini, saya
                                                            harus menerima pekerjaan yang tidak berhubungan dengan pendidikan saya.
                                                            f16-12<br /> <input name="f1613" type="checkbox" value="13" {{(old('f1613') == '4') ? 'checked' : null}} /> [13]
                                                            Lainnya: f16-13<br /> <input class="form-control" maxlength="80" name="f1614" size="60"
                                                                type="text" value="{{old('f1614')}}" />f16-14</td>
                                                    </tr>
                                                    <tr>
                                                        <td>f17</td>
                                                        <td>Pada saat lulus, pada tingkat mana kompetensi di bawah ini anda kuasai?
                                                            (<strong>A</strong>) <br /> Pada saat ini, pada tingkat mana kompetensi
                                                            di bawah ini diperlukan dalam pekerjaan? (<strong>B</strong>)</td>
                                                        <td>:</td>
                                                        <td>
                                                            <table>
                                                                <tbody>
                                                                    <tr>
                                                                        <th colspan="5">A</th>
                                                                        <th colspan="1"></th>
                                                                        <th colspan="5">B</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th colspan="2">Sangat Rendah</th>
                                                                        <th colspan="2">Sangat Tinggi</th>
                                                                        <th colspan="2"></th>
                                                                        <th colspan="2">Sangat Rendah</th>
                                                                        <th colspan="2">Sangat Tinggi</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>1</th>
                                                                        <th>2</th>
                                                                        <th>3</th>
                                                                        <th>4</th>
                                                                        <th>5</th>
                                                                        <th colspan="1"></th>
                                                                        <th>1</th>
                                                                        <th>2</th>
                                                                        <th>3</th>
                                                                        <th>4</th>
                                                                        <th>5</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1701" type="radio" value="1" {{(old('f1701') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1701" type="radio" value="2" {{(old('f1701') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1701" type="radio" value="3" {{(old('f1701') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1701" type="radio" value="4" {{(old('f1701') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1701" type="radio" value="5" {{(old('f1701') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Pengetahuan di bidang atau disiplin ilmu anda f17-1
                                                                            f17-2b</td>
                                                                        <td><input name="f1702b" type="radio" value="1" {{(old('f1702b') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1702b" type="radio" value="2" {{(old('f1702b') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1702b" type="radio" value="3" {{(old('f1702b') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1702b" type="radio" value="4" {{(old('f1702b') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1702b" type="radio" value="5" {{(old('f1702b') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1703" type="radio" value="1" {{(old('f1703') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1703" type="radio" value="2" {{(old('f1703') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1703" type="radio" value="3" {{(old('f1703') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1703" type="radio" value="4" {{(old('f1703') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1703" type="radio" value="5" {{(old('f1703') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Pengetahuan di luar bidang atau disiplin ilmu anda f17-3
                                                                            f17-4b</td>
                                                                        <td><input name="f1704b" type="radio" value="1" {{(old('f1704b') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1704b" type="radio" value="2" {{(old('f1704b') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1704b" type="radio" value="3" {{(old('f1704b') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1704b" type="radio" value="4" {{(old('f1704b') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1704b" type="radio" value="5" {{(old('f1704b') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1705" type="radio" value="1" {{(old('f1705') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1705" type="radio" value="2" {{(old('f1705') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1705" type="radio" value="3" {{(old('f1705') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1705" type="radio" value="4" {{(old('f1705') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1705" type="radio" value="5" {{(old('f1705') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Pengetahuan umum f17-5 f17-6b</td>
                                                                        <td><input name="f1706b" type="radio" value="1" {{(old('f1706b') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1706b" type="radio" value="2" {{(old('f1706b') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1706b" type="radio" value="3" {{(old('f1706b') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1706b" type="radio" value="4" {{(old('f1706b') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1706b" type="radio" value="5" {{(old('f1706b') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1705a" type="radio" value="1" {{(old('f1705a') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1705a" type="radio" value="2" {{(old('f1705a') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1705a" type="radio" value="3" {{(old('f1705a') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1705a" type="radio" value="4" {{(old('f1705a') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1705a" type="radio" value="5" {{(old('f1705a') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Bahasa Inggris f17-5a f17-6ba</td>
                                                                        <td><input name="f1706ba" type="radio" value="1" {{(old('f1706ba') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1706ba" type="radio" value="2" {{(old('f1706ba') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1706ba" type="radio" value="3" {{(old('f1706ba') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1706ba" type="radio" value="4" {{(old('f1706ba') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1706ba" type="radio" value="5" {{(old('f1706ba') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1707" type="radio" value="1" {{(old('f1707') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1707" type="radio" value="2" {{(old('f1707') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1707" type="radio" value="3" {{(old('f1707') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1707" type="radio" value="4" {{(old('f1707') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1707" type="radio" value="5" {{(old('f1707') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Ketrampilan internet f17-7 f17-8b</td>
                                                                        <td><input name="f1708b" type="radio" value="1" {{(old('f1708b') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1708b" type="radio" value="2" {{(old('f1708b') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1708b" type="radio" value="3" {{(old('f1708b') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1708b" type="radio" value="4" {{(old('f1708b') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1708b" type="radio" value="5" {{(old('f1708b') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1709" type="radio" value="1" {{(old('f1709') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1709" type="radio" value="2" {{(old('f1709') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1709" type="radio" value="3" {{(old('f1709') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1709" type="radio" value="4" {{(old('f1709') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1709" type="radio" value="5" {{(old('f1709') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Ketrampilan komputer f17-9 f17-10b</td>
                                                                        <td><input name="f1710b" type="radio" value="1" {{(old('f1710b') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1710b" type="radio" value="2" {{(old('f1710b') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1710b" type="radio" value="3" {{(old('f1710b') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1710b" type="radio" value="4" {{(old('f1710b') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1710b" type="radio" value="5" {{(old('f1710b') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1711" type="radio" value="1" {{(old('f1711') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1711" type="radio" value="2" {{(old('f1711') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1711" type="radio" value="3" {{(old('f1711') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1711" type="radio" value="4" {{(old('f1711') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1711" type="radio" value="5" {{(old('f1711') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Berpikir kritis f17-11 f17-12b</td>
                                                                        <td><input name="f1712b" type="radio" value="1" {{(old('f1712b') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1712b" type="radio" value="2" {{(old('f1712b') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1712b" type="radio" value="3" {{(old('f1712b') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1712b" type="radio" value="4" {{(old('f1712b') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1712b" type="radio" value="5" {{(old('f1712b') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1713" type="radio" value="1" {{(old('f1713') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1713" type="radio" value="2" {{(old('f1713') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1713" type="radio" value="3" {{(old('f1713') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1713" type="radio" value="4" {{(old('f1713') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1713" type="radio" value="5" {{(old('f1713') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Ketrampilan riset f17-13 f17-14b</td>
                                                                        <td><input name="f1714b" type="radio" value="1" {{(old('f1714b') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1714b" type="radio" value="2" {{(old('f1714b') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1714b" type="radio" value="3" {{(old('f1714b') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1714b" type="radio" value="4" {{(old('f1714b') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1714b" type="radio" value="5" {{(old('f1714b') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1715" type="radio" value="1" {{(old('f1715') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1715" type="radio" value="2" {{(old('f1715') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1715" type="radio" value="3" {{(old('f1715') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1715" type="radio" value="4" {{(old('f1715') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1715" type="radio" value="5" {{(old('f1715') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Kemampuan belajar f17-15 f17-16b</td>
                                                                        <td><input name="f1716b" type="radio" value="1" {{(old('f1716b') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1716b" type="radio" value="2" {{(old('f1716b') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1716b" type="radio" value="3" {{(old('f1716b') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1716b" type="radio" value="4" {{(old('f1716b') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1716b" type="radio" value="5" {{(old('f1716b') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1717" type="radio" value="1" {{(old('f1717') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1717" type="radio" value="2" {{(old('f1717') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1717" type="radio" value="3" {{(old('f1717') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1717" type="radio" value="4" {{(old('f1717') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1717" type="radio" value="5" {{(old('f1717') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Kemampuan berkomunikasi f17-17 f17-18b</td>
                                                                        <td><input name="f1718b" type="radio" value="1" {{(old('f1718b') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1718b" type="radio" value="2" {{(old('f1718b') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1718b" type="radio" value="3" {{(old('f1718b') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1718b" type="radio" value="4" {{(old('f1718b') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1718b" type="radio" value="5" {{(old('f1718b') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1719" type="radio" value="1" {{(old('f1719') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1719" type="radio" value="2" {{(old('f1719') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1719" type="radio" value="3" {{(old('f1719') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1719" type="radio" value="4" {{(old('f1719') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1719" type="radio" value="5" {{(old('f1719') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Bekerja di bawah tekanan f17-19 f17-20b</td>
                                                                        <td><input name="f1720b" type="radio" value="1" {{(old('f1720b') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1720b" type="radio" value="2" {{(old('f1720b') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1720b" type="radio" value="3" {{(old('f1720b') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1720b" type="radio" value="4" {{(old('f1720b') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1720b" type="radio" value="5" {{(old('f1720b') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1721" type="radio" value="1" {{(old('f1721') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1721" type="radio" value="2" {{(old('f1721') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1721" type="radio" value="3" {{(old('f1721') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1721" type="radio" value="4" {{(old('f1721') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1721" type="radio" value="5" {{(old('f1721') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Manajemen waktu f17-21 f17-22b</td>
                                                                        <td><input name="f1722b" type="radio" value="1" {{(old('f1722b') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1722b" type="radio" value="2" {{(old('f1722b') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1722b" type="radio" value="3" {{(old('f1722b') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1722b" type="radio" value="4" {{(old('f1722b') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1722b" type="radio" value="5" {{(old('f1722b') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1723" type="radio" value="1" {{(old('f1723') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1723" type="radio" value="2" {{(old('f1723') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1723" type="radio" value="3" {{(old('f1723') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1723" type="radio" value="4" {{(old('f1723') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1723" type="radio" value="5" {{(old('f1723') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Bekerja secara mandiri f17-23 f17-24b</td>
                                                                        <td><input name="f1724b" type="radio" value="1" {{(old('f1724b') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1724b" type="radio" value="2" {{(old('f1724b') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1724b" type="radio" value="3" {{(old('f1724b') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1724b" type="radio" value="4" {{(old('f1724b') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1724b" type="radio" value="5" {{(old('f1724b') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1725" type="radio" value="1" {{(old('f1725') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1725" type="radio" value="2" {{(old('f1725') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1725" type="radio" value="3" {{(old('f1725') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1725" type="radio" value="4" {{(old('f1725') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1725" type="radio" value="5" {{(old('f1725') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Bekerja dalam tim/bekerjasama dengan orang lain f17-25
                                                                            f17-26b</td>
                                                                        <td><input name="f1726b" type="radio" value="1" {{(old('f1726b') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1726b" type="radio" value="2" {{(old('f1726b') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1726b" type="radio" value="3" {{(old('f1726b') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1726b" type="radio" value="4" {{(old('f1726b') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1726b" type="radio" value="5" {{(old('f1726b') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1727" type="radio" value="1" {{(old('f1727') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1727" type="radio" value="2" {{(old('f1727') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1727" type="radio" value="3" {{(old('f1727') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1727" type="radio" value="4" {{(old('f1727') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1727" type="radio" value="5" {{(old('f1727') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Kemampuan dalam memecahkan masalah f17-27 f17-28b</td>
                                                                        <td><input name="f1728b" type="radio" value="1" {{(old('f1728b') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1728b" type="radio" value="2" {{(old('f1728b') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1728b" type="radio" value="3" {{(old('f1728b') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1728b" type="radio" value="4" {{(old('f1728b') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1728b" type="radio" value="5" {{(old('f1728b') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1729" type="radio" value="1" {{(old('f1729') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1729" type="radio" value="2" {{(old('f1729') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1729" type="radio" value="3" {{(old('f1729') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1729" type="radio" value="4" {{(old('f1729') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1729" type="radio" value="5" {{(old('f1729') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Negosiasi f17-29 f17-30b</td>
                                                                        <td><input name="f1730b" type="radio" value="1" {{(old('f1730b') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1730b" type="radio" value="2" {{(old('f1730b') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1730b" type="radio" value="3" {{(old('f1730b') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1730b" type="radio" value="4" {{(old('f1730b') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1730b" type="radio" value="5" {{(old('f1730b') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1731" type="radio" value="1" {{(old('f1731') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1731" type="radio" value="2" {{(old('f1731') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1731" type="radio" value="3" {{(old('f1731') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1731" type="radio" value="4" {{(old('f1731') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1731" type="radio" value="5" {{(old('f1731') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Kemampuan analisis f17-31 f17-32b</td>
                                                                        <td><input name="f1732b" type="radio" value="1" {{(old('f1732b') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1732b" type="radio" value="2" {{(old('f1732b') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1732b" type="radio" value="3" {{(old('f1732b') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1732b" type="radio" value="4" {{(old('f1732b') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1732b" type="radio" value="5" {{(old('f1732b') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1733" type="radio" value="1" {{(old('f1733') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1733" type="radio" value="2" {{(old('f1733') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1733" type="radio" value="3" {{(old('f1733') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1733" type="radio" value="4" {{(old('f1733') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1733" type="radio" value="5" {{(old('f1733') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Toleransi f17-33 f17-34b</td>
                                                                        <td><input name="f1734b" type="radio" value="1" {{(old('f1734b') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1734b" type="radio" value="2" {{(old('f1734b') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1734b" type="radio" value="3" {{(old('f1734b') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1734b" type="radio" value="4" {{(old('f1734b') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1734b" type="radio" value="5" {{(old('f1734b') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1735" type="radio" value="1" {{(old('f1735') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1735" type="radio" value="2" {{(old('f1735') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1735" type="radio" value="3" {{(old('f1735') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1735" type="radio" value="4" {{(old('f1735') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1735" type="radio" value="5" {{(old('f1735') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Kemampuan adaptasi f17-35 f17-36b</td>
                                                                        <td><input name="f1736b" type="radio" value="1" {{(old('f1736b') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1736b" type="radio" value="2" {{(old('f1736b') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1736b" type="radio" value="3" {{(old('f1736b') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1736b" type="radio" value="4" {{(old('f1736b') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1736b" type="radio" value="5" {{(old('f1736b') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1737" type="radio" value="1" {{(old('f1737') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1737" type="radio" value="2" {{(old('f1737') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1737" type="radio" value="3" {{(old('f1737') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1737" type="radio" value="4" {{(old('f1737') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1737" type="radio" value="5" {{(old('f1737') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Loyalitasf17-37 f17-38b</td>
                                                                        <td><input name="f1738b" type="radio" value="1" {{(old('f1738b') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1738b" type="radio" value="2" {{(old('f1738b') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1738b" type="radio" value="3" {{(old('f1738b') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1738b" type="radio" value="4" {{(old('f1738b') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1738b" type="radio" value="5" {{(old('f1738b') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1737a" type="radio" value="1" {{(old('f1737a') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1737a" type="radio" value="2" {{(old('f1737a') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1737a" type="radio" value="3" {{(old('f1737a') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1737a" type="radio" value="4" {{(old('f1737a') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1737a" type="radio" value="5" {{(old('f1737a') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Integritas f17-37A f17-38ba</td>
                                                                        <td><input name="f1738ba" type="radio" value="1" {{(old('f1738ba') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1738ba" type="radio" value="2" {{(old('f1738ba') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1738ba" type="radio" value="3" {{(old('f1738ba') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1738ba" type="radio" value="4" {{(old('f1738ba') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1738ba" type="radio" value="5" {{(old('f1738ba') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1739" type="radio" value="1" {{(old('f1739') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1739" type="radio" value="2" {{(old('f1739') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1739" type="radio" value="3" {{(old('f1739') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1739" type="radio" value="4" {{(old('f1739') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1739" type="radio" value="5" {{(old('f1739') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Bekerja dengan orang yang berbeda budaya maupun latar
                                                                            belakang f17-39 f17-40b</td>
                                                                        <td><input name="f1740b" type="radio" value="1" {{(old('f1740b') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1740b" type="radio" value="2" {{(old('f1740b') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1740b" type="radio" value="3" {{(old('f1740b') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1740b" type="radio" value="4" {{(old('f1740b') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1740b" type="radio" value="5" {{(old('f1740b') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1741" type="radio" value="1" {{(old('f1741') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1741" type="radio" value="2" {{(old('f1741') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1741" type="radio" value="3" {{(old('f1741') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1741" type="radio" value="4" {{(old('f1741') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1741" type="radio" value="5" {{(old('f1741') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Kepemimpinan f17-41 f17-42b</td>
                                                                        <td><input name="f1742b" type="radio" value="1" {{(old('f1742b') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1742b" type="radio" value="2" {{(old('f1742b') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1742b" type="radio" value="3" {{(old('f1742b') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1742b" type="radio" value="4" {{(old('f1742b') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1742b" type="radio" value="5" {{(old('f1742b') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1743" type="radio" value="1" {{(old('f1743') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1743" type="radio" value="2" {{(old('f1743') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1743" type="radio" value="3" {{(old('f1743') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1743" type="radio" value="4" {{(old('f1743') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1743" type="radio" value="5" {{(old('f1743') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Kemampuan dalam memegang tanggungjawab f17-43 f17-44b
                                                                        </td>
                                                                        <td><input name="f1744b" type="radio" value="1" {{(old('f1744b') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1744b" type="radio" value="2" {{(old('f1744b') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1744b" type="radio" value="3" {{(old('f1744b') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1744b" type="radio" value="4" {{(old('f1744b') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1744b" type="radio" value="5" {{(old('f1744b') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1745" type="radio" value="1" {{(old('f1745') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1745" type="radio" value="2" {{(old('f1745') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1745" type="radio" value="3" {{(old('f1745') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1745" type="radio" value="4" {{(old('f1745') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1745" type="radio" value="5" {{(old('f1745') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Inisiatif f17-45 f17-46b</td>
                                                                        <td><input name="f1746b" type="radio" value="1" {{(old('f1746b') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1746b" type="radio" value="2" {{(old('f1746b') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1746b" type="radio" value="3" {{(old('f1746b') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1746b" type="radio" value="4" {{(old('f1746b') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1746b" type="radio" value="5" {{(old('f1746b') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1747" type="radio" value="1" {{(old('f1747') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1747" type="radio" value="2" {{(old('f1747') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1747" type="radio" value="3" {{(old('f1747') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1747" type="radio" value="4" {{(old('f1747') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1747" type="radio" value="5" {{(old('f1747') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Manajemen proyek/program f17-47 f17-48b</td>
                                                                        <td><input name="f1748b" type="radio" value="1" {{(old('f1748b') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1748b" type="radio" value="2" {{(old('f1748b') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1748b" type="radio" value="3" {{(old('f1748b') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1748b" type="radio" value="4" {{(old('f1748b') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1748b" type="radio" value="5" {{(old('f1748b') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1749" type="radio" value="1" {{(old('f1749') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1749" type="radio" value="2" {{(old('f1749') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1749" type="radio" value="3" {{(old('f1749') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1749" type="radio" value="4" {{(old('f1749') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1749" type="radio" value="5" {{(old('f1749') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Kemampuan untuk memresentasikan ide/produk/laporan
                                                                            f17-49 f17-50b</td>
                                                                        <td><input name="f1750b" type="radio" value="1" {{(old('f1750b') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1750b" type="radio" value="2" {{(old('f1750b') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1750b" type="radio" value="3" {{(old('f1750b') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1750b" type="radio" value="4" {{(old('f1750b') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1750b" type="radio" value="5" {{(old('f1750b') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1751" type="radio" value="1" {{(old('f1751') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1751" type="radio" value="2" {{(old('f1751') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1751" type="radio" value="3" {{(old('f1751') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1751" type="radio" value="4" {{(old('f1751') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1751" type="radio" value="5" {{(old('f1751') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Kemampuan dalam menulis laporan, memo dan dokumen f17-51
                                                                            f17-52b</td>
                                                                        <td><input name="f1752b" type="radio" value="1" {{(old('f1752b') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1752b" type="radio" value="2" {{(old('f1752b') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1752b" type="radio" value="3" {{(old('f1752b') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1752b" type="radio" value="4" {{(old('f1752b') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1752b" type="radio" value="5" {{(old('f1752b') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input name="f1753" type="radio" value="1" {{(old('f1753') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1753" type="radio" value="2" {{(old('f1753') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1753" type="radio" value="3" {{(old('f1753') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1753" type="radio" value="4" {{(old('f1753') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1753" type="radio" value="5" {{(old('f1753') == '5') ? 'checked' : null}}/></td>
                                                                        <td>Kemampuan untuk terus belajar sepanjang hayat f17-53
                                                                            f17-54b</td>
                                                                        <td><input name="f1754b" type="radio" value="1" {{(old('f1754b') == '1') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1754b" type="radio" value="2" {{(old('f1754b') == '2') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1754b" type="radio" value="3" {{(old('f1754b') == '3') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1754b" type="radio" value="4" {{(old('f1754b') == '4') ? 'checked' : null}}/></td>
                                                                        <td><input name="f1754b" type="radio" value="5" {{(old('f1754b') == '5') ? 'checked' : null}}/></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <button class="btn btn-primary" type="submit"><i data-feather="file-plus" class="icon-xs"></i> Submit Tracer</button>
                                        </form>
                                    </div>
                                @else 
                                    <div class="row align-items-center">
                                        <div class="col-xl-2 col-lg-3 col-6">
                                            <img src="{{asset('assets/back/images/success.png')}}" class="mr-4 align-self-center img-fluid "
                                            alt="cal" />
                                        </div>
                                        <div class="col-xl-10 col-lg-9">
                                            <div class="mt-4 mt-lg-0">
                                                <h5 class="mt-0 mb-1 font-weight-bold text-success">Data berhasil ditambahkan <i data-feather="check-circle"></i> </h5>
                                                <p class="text-muted mb-2">Terimakasih banyak {!! '<b>'.$_GET['nama_lengkap'].'</b>' !!} sudah mengisi formulir tracer, informasi selanjutnya akan dihubungi melalui no telp/whatsapp terdaftar. Untuk informasi lainnya terkait tracer study bisa menghubungi kontak whatsapp dibawah.</p>

                                                <a href="https://wa.me/6285220717928" class="btn btn-success mt-2 mr-1" id="btn-new-event"><i
                                                        class="uil-whatsapp"></i> Contact (Irfan Fadil)</a>
                                                <a href="{{url('tracers/create')}}" class="btn btn-white mt-2"><i class="uil-sync"></i> Isi Kembali ?</a>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div> <!-- end col-->
                </div>

            </div> <!-- container-fluid -->

        </div> <!-- content -->
    @endsection
