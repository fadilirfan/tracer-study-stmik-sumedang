<?php
$url = explode("/",url()->current());
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>{{$title}}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Tracer Study STMIK Sumedang" name="Tracer Study STMIK Sumedang" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{asset('assets/back/images/favicon.ico')}}">

        <!-- plugins -->
        <link href="{{asset('assets/back/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
        @yield('css')

        <!-- App css -->
        <link href="{{asset('assets/back/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/back/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/back/css/app.min.css')}}" rel="stylesheet" type="text/css" />

    </head>

    <body>
        <!-- Begin page -->
        <div id="wrapper">

            <!-- Topbar Start -->
            <div class="navbar navbar-expand flex-column flex-md-row navbar-custom">
                <div class="container-fluid">
                    <!-- LOGO -->
                    <a href="{{ url('/')}}" class="navbar-brand mr-0 mr-md-2 logo">
                        <span class="logo-lg">
                            <img src="{{asset('assets/back/images/logo.png')}}" alt="" height="50" />
                        </span>
                        <span class="logo-sm">
                            <img src="{{asset('assets/back/images/logo.png')}}" alt="" height="50">
                        </span>
                    </a>

                    <ul class="navbar-nav bd-navbar-nav flex-row list-unstyled menu-left mb-0">
                        <li class="">
                            <button class="button-menu-mobile open-left disable-btn">
                                <i data-feather="menu" class="menu-icon"></i>
                                <i data-feather="x" class="close-icon"></i>
                            </button>
                        </li>
                    </ul>
                </div>

            </div>
            <!-- end Topbar -->

            @if (Auth::check())
                <!-- ========== Left Sidebar Start ========== -->
                <div class="left-side-menu">
                    <div class="media user-profile mt-2 mb-2">
                        @php
                            $path = asset('input/users/profile/default.png');
                            if (!empty(Auth::user()->picture)) {
                                $path = Storage::url(Auth::user()->picture);
                            }
                            $fullname = 'Civitas';
                            if (!empty(Auth::user()->fullname)) {
                                $fullname = Auth::user()->fullname;
                            }
                            $user_level = 'Tamu';
                            if (!empty(Auth::user()->user_level->name)) {
                                $user_level = Auth::user()->user_level->name;
                            }
                        @endphp
                        <img src="{{$path}}" class="avatar-sm rounded-circle mr-2" alt="{{$fullname}}" />
                        <img src="{{$path}}" class="avatar-xs rounded-circle mr-2" alt="{{$fullname}}" />

                        <div class="media-body">
                            <h6 class="pro-user-name mt-0 mb-0">{{$fullname}}</h6>
                            <span class="pro-user-desc">{{$user_level}}</span>
                        </div>
                        @if (Auth::check())
                            <div class="dropdown align-self-center profile-dropdown-menu">
                                <a class="dropdown-toggle mr-0" data-toggle="dropdown" href="#" role="button" aria-haspopup="false"
                                    aria-expanded="false">
                                    <span data-feather="chevron-down"></span>
                                </a>
                                <div class="dropdown-menu profile-dropdown">

                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i data-feather="settings" class="icon-dual icon-xs mr-2"></i>
                                        <span>Settings</span>
                                    </a>

                                    <div class="dropdown-divider"></div>
                                    

                                    <a class="dropdown-item notify-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="sidebar-content">
                        <!--- Sidemenu -->
                        <div id="sidebar-menu" class="slimscroll-menu">
                            <ul class="metismenu" id="menu-bar">
                                <li class="menu-title">Navigation</li>

                                <li >
                                    <a href="{{url('home')}}" class="{{($url[3] == 'home') ? 'active' : null}}">
                                        <i data-feather="home"></i>
                                        <span> Dashboard </span>
                                    </a>   
                                </li>
                                @if (Auth::check())
                                    <li class="menu-title">User</li>
                                    <li>
                                        <a href="{{url('users')}}" class="{{($url[3] == 'users') ? 'active' : null}}">
                                            <i data-feather="users"></i>
                                            <span> User </span>
                                        </a>
                                    </li>
                                @endif

                                <li class="menu-title">Main Data</li>
                                <li>
                                    <a href="{{url('kuesioners')}}" class="{{($url[3] == 'kuesioners') ? 'active' : null}}">
                                        <i data-feather="file-text"></i>
                                        <span> Kuesioner </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('tracers')}}" class="{{($url[3] == 'tracers') ? 'active' : null}}">
                                        <i data-feather="inbox"></i>
                                        <span> Tracer </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- End Sidebar -->

                        <div class="clearfix"></div>
                    </div>
                    <!-- Sidebar -left -->

                </div>
                <!-- Left Sidebar End -->
            @endif

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            @yield('content')

            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->
            <!-- Footer Start -->
            <footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            2021 &copy; Tracer Study. All Rights Reserved. Crafted with <i class='uil uil-heart text-warning font-size-12'></i> by <a href="https://stmik-sumedang.ac.id" target="_blank">STMIK Sumedang</a>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- end Footer -->


        </div>
        <!-- END wrapper -->

        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- Vendor js -->
        <script src="{{asset('assets/back/js/vendor.min.js')}}"></script>

        <!-- optional plugins -->
        <script src="{{asset('assets/back/libs/moment/moment.min.js')}}"></script>
        <script src="{{asset('assets/back/libs/apexcharts/apexcharts.min.js')}}"></script>
        <script src="{{asset('assets/back/libs/flatpickr/flatpickr.min.js')}}"></script>

        <!-- page js -->
        <script src="{{asset('assets/back/js/pages/dashboard.init.js')}}"></script>

        @yield('js')

        <!-- App js -->
        <script src="{{asset('assets/back/js/app.min.js')}}"></script>


    </body>
</html>