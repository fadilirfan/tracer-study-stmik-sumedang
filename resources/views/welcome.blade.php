<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Tracer Study STMIK Sumedang</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Tracer Study STMIK Sumedang" name="Tracer Study STMIK Sumedang" />
    <meta content="STMIK Sumdang" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('')}}assets/back/images/favicon.ico">

    <!-- App css -->
    <link href="{{asset('')}}assets/back/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="{{asset('')}}assets/back/css/icons.min.css" rel="stylesheet" type="text/css" />
    <link href="{{asset('')}}assets/back/css/app.min.css" rel="stylesheet" type="text/css" />

    <style>
        .circles {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            overflow: hidden
        }

        .circles li {
            position: absolute;
            display: block;
            list-style: none;
            width: 20px;
            height: 20px;
            background: rgba(255, 255, 255, .7);
            animation: animate 25s linear infinite;
            bottom: -150px
        }

        .circles li:nth-child(1) {
            left: 25%;
            width: 80px;
            height: 80px;
            animation-delay: 0s
        }

        .circles li:nth-child(2) {
            left: 10%;
            width: 20px;
            height: 20px;
            animation-delay: 2s;
            animation-duration: 12s
        }

        .circles li:nth-child(3) {
            left: 70%;
            width: 20px;
            height: 20px;
            animation-delay: 4s
        }

        .circles li:nth-child(4) {
            left: 40%;
            width: 60px;
            height: 60px;
            animation-delay: 0s;
            animation-duration: 18s
        }

        .circles li:nth-child(5) {
            left: 65%;
            width: 20px;
            height: 20px;
            animation-delay: 0s
        }

        .circles li:nth-child(6) {
            left: 75%;
            width: 110px;
            height: 110px;
            animation-delay: 3s
        }

        .circles li:nth-child(7) {
            left: 35%;
            width: 150px;
            height: 150px;
            animation-delay: 7s
        }

        .circles li:nth-child(8) {
            left: 50%;
            width: 25px;
            height: 25px;
            animation-delay: 15s;
            animation-duration: 45s
        }

        .circles li:nth-child(9) {
            left: 20%;
            width: 15px;
            height: 15px;
            animation-delay: 2s;
            animation-duration: 35s
        }

        .circles li:nth-child(10) {
            left: 85%;
            width: 150px;
            height: 150px;
            animation-delay: 0s;
            animation-duration: 11s
        }

        @keyframes animate {
            0% {
                transform: translateY(0) rotate(0);
                opacity: 1;
                border-radius: 0
            }

            100% {
                transform: translateY(-1000px) rotate(720deg);
                opacity: 0;
                border-radius: 50%
            }
        }

        .font-size-44 {
            font-size: 44px
        }

        .animate {
            -webkit-transform: scale(1);
            transform: scale(1);
            -webkit-transition: .3s ease-in-out;
            transition: .3s ease-in-out
        }

        .animate:hover {
            -webkit-transform: scale(1.1);
            transform: scale(1.1)
        }
    </style>

</head>

<body class="pb-0">

    
    <!-- NAVBAR START -->
    <nav class="navbar navbar-expand-lg py-lg-3 mb-2 sticky-top" id="nav-menu">
        <div class="container">
            <!-- logo -->
            <a href="{{ url('/')}}" class="navbar-brand mr-lg-5 font-size-22 font-weight-bold text-dark">
                <img src="{{asset('')}}assets/back/images/logo.png" alt="" class="logo-dark mr-2" height="50" />
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <i class='uil uil-bars'></i>
            </button>

            <!-- menus -->
            <div class="collapse navbar-collapse" id="navbarNavDropdown">

                <!-- left menu -->
                <ul class="navbar-nav mr-auto align-items-center">

                </ul>

                <!-- right menu -->
                <ul class="navbar-nav ml-auto align-items-center font-size-16 font-weight-bold">
                    <li class="nav-item mr-0 ml-4">
                        @if (Route::has('login'))
                            <div class="top-right links">
                                @auth
                                    <a href="{{ url('/') }}">Home</a>
                                @else
                                    <a href="{{ url('/') }}">Home</a>

                                    {{-- @if (Route::has('register'))
                                        <a href="{{ route('register') }}">Register</a>
                                    @endif --}}
                                @endauth
                            </div>
                        @endif
                    </li>
                    <li class="nav-item mr-0 ml-4">
                            <div class="top-right links">
                                <a href="{{ url('/kuesioners') }}">Kuesioner</a>
                                
                            </div>
                        
                    </li>
                    <li class="nav-item mr-0 ml-4">
                        <div class="top-right links">
                            <a href="{{ url('/tracers') }}">Tracer</a>
                            
                        </div>
                    
                </li>
                </ul>

            </div>
        </div>
    </nav>
    <!-- NAVBAR END -->

    <!-- START HERO -->
    <section class="hero-section position-relative overflow-hidden px-3 text-dark pt-5 pb-0"
        style="background: url('{{asset('')}}assets/back/images/covers/pattern.png') center top;">
        <ul class="circles">
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
        </ul>

        <div class="container">
            <div class="row align-items-center">
                <div class="col text-center">
                    <div class="py-5">
                        <h1 class="mb-4">
                            Selamat Datang di Official Website Tracer Study STMIK Sumedang
                        </h1>

                        <p class="mb-4 font-size-16 text-center">
                            Halaman ini diperuntukkan untuk mengelola data hasil tracer study yang dilaksanakan oleh STMIK Sumedang. 
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <img src="{{asset('')}}assets/back/images/layouts/vertical-tracer.png" alt=""
                        class="mx-auto d-block shadow img-fluid" height="50%" />
                </div>
            </div>
        </div>
    </section>
    <!-- END HERO -->


    <!-- START FOOTER -->
    <footer class="pt-5 pb-3 position-relative" style="background: url('{{asset('')}}assets/back/images/covers/pattern.png') center top;">
        <ul class="circles">
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
        </ul>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <a href="index.html" class="navbar-brand mr-lg-5 font-size-22 font-weight-bold text-dark">
                        <img src="{{asset('')}}assets/back/images/logo.png" alt="" class="logo-dark mr-2" height="80" />
                    </a>
                    <p class="font-size-15 mt-4">Tracer Study merupakan sebuah aplikasi pengelola database alumni STMIK Sumedang.
                </div>

                <div class="col-lg-2">

                </div>

                <div class="col-lg-2">
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="mt-5">
                        <p class="mt-4 text-center mb-0">© 2021 STMIK Sumedang</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- END FOOTER -->


    <!-- Vendor js -->
    <script src="{{asset('')}}assets/back/js/vendor.min.js"></script>
    <!-- App js -->
    <script src="{{asset('')}}assets/back/js/app.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(".link-demos").click(function () {
                $('html, body').animate({
                    scrollTop: $("#demos").offset().top
                }, 1000);
            });

            // make the navbar stikcy
            var nav = $("#nav-menu");
            $(window).on('scroll', function(e) {
                if (window.scrollY > 50) {
                    nav.addClass('bg-white');
                    nav.addClass('shadow');
                } else {
                    nav.removeClass('bg-white');
                    nav.removeClass('shadow');
                }
            });
        });
    </script>
</body>

</html>
