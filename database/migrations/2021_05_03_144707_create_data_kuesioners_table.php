<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataKuesionersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_kuesioners', function (Blueprint $table) {
            $table->id();
            $table->string('nimhsmsmh');
            $table->string('kdptimsmh');
            $table->string('tahun_lulus');
            $table->string('kdpstmsmh');
            $table->string('nmmhsmsmh');
            $table->string('telpomsmh');
            $table->string('emailmsmh');
            $table->string('nik');
            $table->string('npwp');
            $table->integer('f8')->default(0);
            $table->integer('f504')->default(0);
            $table->integer('f502')->default(0);
            $table->string('f505')->nullable();
            $table->integer('f506')->default(0);
            $table->char('f5a1', 2);
            $table->char('f5a2', 4)->default(0);
            $table->integer('f1101')->default(0);
            $table->string('f1102')->nullable();
            $table->string('f5b')->nullable();
            $table->integer('f5c')->default(0);
            $table->integer('f5d')->default(0);
            $table->integer('f18a')->default(0);
            $table->integer('f18b')->default(0);
            $table->integer('f18c')->default(0);
            $table->date('f18d');
            $table->integer('f1201')->default(0);
            $table->string('f1202')->nullable();
            $table->integer('f14')->default(0);
            $table->integer('f15')->default(0);
            $table->integer('f1761')->default(0);
            $table->integer('f1762')->default(0);
            $table->integer('f1763')->default(0);
            $table->integer('f1764')->default(0);
            $table->integer('f1765')->default(0);
            $table->integer('f1766')->default(0);
            $table->integer('f1767')->default(0);
            $table->integer('f1768')->default(0);
            $table->integer('f1769')->default(0);
            $table->integer('f1770')->default(0);
            $table->integer('f1771')->default(0);
            $table->integer('f1772')->default(0);
            $table->integer('f1773')->default(0);
            $table->integer('f1774')->default(0);
            $table->integer('f21')->default(0);
            $table->integer('f22')->default(0);
            $table->integer('f23')->default(0);
            $table->integer('f24')->default(0);
            $table->integer('f25')->default(0);
            $table->integer('f26')->default(0);
            $table->integer('f27')->default(0);
            $table->integer('f301')->default(0);
            $table->string('f302')->nullable();
            $table->string('f303')->nullable();
            $table->integer('f401')->default(0);
            $table->integer('f402')->default(0);
            $table->integer('f403')->default(0);
            $table->integer('f404')->default(0);
            $table->integer('f405')->default(0);
            $table->integer('f406')->default(0);
            $table->integer('f407')->default(0);
            $table->integer('f408')->default(0);
            $table->integer('f409')->default(0);
            $table->integer('f410')->default(0);
            $table->integer('f411')->default(0);
            $table->integer('f412')->default(0);
            $table->integer('f413')->default(0);
            $table->integer('f414')->default(0);
            $table->integer('f415')->default(0);
            $table->string('f416')->nullable();
            $table->integer('f6')->default(0);
            $table->integer('f7')->default(0);
            $table->integer('f7a')->default(0);
            $table->integer('f1001')->default(0);
            $table->string('f1002')->nullable();
            $table->integer('f1601')->default(0);
            $table->integer('f1602')->default(0);
            $table->integer('f1603')->default(0);
            $table->integer('f1604')->default(0);
            $table->integer('f1605')->default(0);
            $table->integer('f1606')->default(0);
            $table->integer('f1607')->default(0);
            $table->integer('f1608')->default(0);
            $table->integer('f1609')->default(0);
            $table->integer('f1610')->default(0);
            $table->integer('f1611')->default(0);
            $table->integer('f1612')->default(0);
            $table->integer('f1613')->default(0);
            $table->string('f1614')->nullable();
            $table->timestamps();
            $table->foreign('f5a1')->references('id')->on('indoregion_provinces');
            $table->foreign('f5a2')->references('id')->on('indoregion_regencies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_kuesioners');
    }
}
