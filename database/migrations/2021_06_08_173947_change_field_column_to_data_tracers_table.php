<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeFieldColumnToDataTracersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('data_tracers', function (Blueprint $table) {
            $table->string('nimhsmsmh')->nullable()->change();
            $table->string('kdptimsmh')->nullable()->change();
            $table->string('tahun_lulus')->nullable()->change();
            $table->string('kdpstmsmh')->nullable()->change();
            $table->string('nmmhsmsmh')->nullable()->change();
            $table->string('telpomsmh')->nullable()->change();
            $table->string('emailmsmh')->nullable()->change();
            $table->integer('f501')->nullable()->change();
            $table->integer('f1201')->nullable()->change();
            $table->integer('f8')->nullable()->change();
            $table->integer('f14')->nullable()->change();
            $table->integer('f15')->nullable()->change();
            $table->string('f1301')->nullable()->change();
            $table->string('f1302')->nullable()->change();
            $table->string('f1303')->nullable()->change();
            $table->integer('f21')->nullable()->change();
            $table->integer('f22')->nullable()->change();
            $table->integer('f23')->nullable()->change();
            $table->integer('f24')->nullable()->change();
            $table->integer('f25')->nullable()->change();
            $table->integer('f26')->nullable()->change();
            $table->integer('f27')->nullable()->change();
            $table->integer('f301')->nullable()->change();
            $table->integer('f401')->nullable()->change();
            $table->integer('f402')->nullable()->change();
            $table->integer('f403')->nullable()->change();
            $table->integer('f404')->nullable()->change();
            $table->integer('f405')->nullable()->change();
            $table->integer('f406')->nullable()->change();
            $table->integer('f407')->nullable()->change();
            $table->integer('f408')->nullable()->change();
            $table->integer('f409')->nullable()->change();
            $table->integer('f410')->nullable()->change();
            $table->integer('f411')->nullable()->change();
            $table->integer('f412')->nullable()->change();
            $table->integer('f413')->nullable()->change();
            $table->integer('f414')->nullable()->change();
            $table->integer('f415')->nullable()->change();
            $table->integer('f6')->nullable()->change();
            $table->integer('f7')->nullable()->change();
            $table->integer('f7a')->nullable()->change();
            $table->integer('f901')->nullable()->change();
            $table->integer('f902')->nullable()->change();
            $table->integer('f903')->nullable()->change();
            $table->integer('f904')->nullable()->change();
            $table->integer('f905')->nullable()->change();
            $table->integer('f1001')->nullable()->change();
            $table->integer('f1101')->nullable()->change();
            $table->integer('f1601')->nullable()->change();
            $table->integer('f1602')->nullable()->change();
            $table->integer('f1603')->nullable()->change();
            $table->integer('f1604')->nullable()->change();
            $table->integer('f1605')->nullable()->change();
            $table->integer('f1606')->nullable()->change();
            $table->integer('f1607')->nullable()->change();
            $table->integer('f1608')->nullable()->change();
            $table->integer('f1609')->nullable()->change();
            $table->integer('f1610')->nullable()->change();
            $table->integer('f1611')->nullable()->change();
            $table->integer('f1612')->nullable()->change();
            $table->integer('f1613')->nullable()->change();
            $table->integer('f1701')->nullable()->change();
            $table->integer('f1702b')->nullable()->change();
            $table->integer('f1703')->nullable()->change();
            $table->integer('f1704b')->nullable()->change();
            $table->integer('f1705')->nullable()->change();
            $table->integer('f1706b')->nullable()->change();
            $table->integer('f1707')->nullable()->change();
            $table->integer('f1708b')->nullable()->change();
            $table->integer('f1709')->nullable()->change();
            $table->integer('f1710b')->nullable()->change();
            $table->integer('f1711')->nullable()->change();
            $table->integer('f1712b')->nullable()->change();
            $table->integer('f1713')->nullable()->change();
            $table->integer('f1714b')->nullable()->change();
            $table->integer('f1715')->nullable()->change();
            $table->integer('f1716b')->nullable()->change();
            $table->integer('f1717')->nullable()->change();
            $table->integer('f1718b')->nullable()->change();
            $table->integer('f1719')->nullable()->change();
            $table->integer('f1720b')->nullable()->change();
            $table->integer('f1721')->nullable()->change();
            $table->integer('f1722b')->nullable()->change();
            $table->integer('f1723')->nullable()->change();
            $table->integer('f1724b')->nullable()->change();
            $table->integer('f1725')->nullable()->change();
            $table->integer('f1726b')->nullable()->change();
            $table->integer('f1727')->nullable()->change();
            $table->integer('f1728b')->nullable()->change();
            $table->integer('f1729')->nullable()->change();
            $table->integer('f1730b')->nullable()->change();
            $table->integer('f1731')->nullable()->change();
            $table->integer('f1732b')->nullable()->change();
            $table->integer('f1733')->nullable()->change();
            $table->integer('f1734b')->nullable()->change();
            $table->integer('f1735')->nullable()->change();
            $table->integer('f1736b')->nullable()->change();
            $table->integer('f1737')->nullable()->change();
            $table->integer('f1738b')->nullable()->change();
            $table->integer('f1739')->nullable()->change();
            $table->integer('f1740b')->nullable()->change();
            $table->integer('f1741')->nullable()->change();
            $table->integer('f1742b')->nullable()->change();
            $table->integer('f1743')->nullable()->change();
            $table->integer('f1744b')->nullable()->change();
            $table->integer('f1745')->nullable()->change();
            $table->integer('f1746b')->nullable()->change();
            $table->integer('f1747')->nullable()->change();
            $table->integer('f1748b')->nullable()->change();
            $table->integer('f1749')->nullable()->change();
            $table->integer('f1750b')->nullable()->change();
            $table->integer('f1751')->nullable()->change();
            $table->integer('f1752b')->nullable()->change();
            $table->integer('f1753')->nullable()->change();
            $table->integer('f1754b')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('data_tracers', function (Blueprint $table) {
            //
        });
    }
}
