<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tracer;
use App\Exports\TracersExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;

class TracerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth')->except(['index','create','store']);
    }

    public function index()
    {
        if (!Auth::check()) {
            return redirect(route('tracers.create'));die;
        }
        $data['title'] = "Data Tracer";
        $data['tracers'] = Tracer::all();

        return view('back.tracers.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = "Tambah Tracer";
        
        return view('back.tracers.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nimhsmsmh' => 'required',
            'kdptimsmh' => 'required',
            'tahun_lulus' => 'required',
            'kdpstmsmh' => 'required',
            'nmmhsmsmh' => 'required',
            'telpomsmh' => 'required',
            'whatsapp' => 'required',
            'emailmsmh' => 'required',
        ]);

        $tracer = new Tracer($request->all());

        $tracer->save();
        if (Auth::check()) {
            return redirect('/tracers')->with('success', 'Data berhasil ditambahkan!');
        }else{
            return redirect()->route('tracers.create', ['nama_lengkap' => $request->get('nmmhsmsmh')])->with('success', 'Terimakasih banyak sudah mengisi formulir, untuk informasi selanjutnya nanti akan dihubungi melalui no telp/whatsapp terdaftar. Untuk informasi lainnya terkait tracer study bisa menghubungi melalui whatsapp +6285220717928 (Irfan Fadil)');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = "Edit Tracer";
        $data['detail'] = Tracer::findOrFail($id)->first();
        
        return view('back.tracers.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nimhsmsmh' => 'required',
            'kdptimsmh' => 'required',
            'tahun_lulus' => 'required',
            'kdpstmsmh' => 'required',
            'nmmhsmsmh' => 'required',
            'telpomsmh' => 'required',
            'whatsapp' => 'required',
            'emailmsmh' => 'required',
        ]);

        $tracer = Tracer::findOrFail($id);

        $data = $request->all();

        $tracer->update($data);

        return redirect('/tracers')->with('success', 'Data berhasil diperbaharui!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tracer = Tracer::findOrFail($id);

        if ($tracer->delete()) {
            return redirect('/tracers')->with('success', 'Data berhasil dihapus!');
        }else{
            return redirect('/tracers')->with('error', 'Data gagal dihapus!');
        }
    }

    public function export() 
    {
        return Excel::download(new TracersExport, 'tracers.xlsx');
    }
}
