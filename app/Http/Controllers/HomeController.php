<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tracer;
use App\Kuesioner;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['title'] = 'Dashboard';
        $data['tracer_count'] = Tracer::count(); 
        $data['kuesioner_count'] = Kuesioner::count(); 
        
        return view('back.dashboard', $data);
    }
}
