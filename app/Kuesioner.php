<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kuesioner extends Model
{
    protected $table = 'data_kuesioners';

    protected $guarded = ['id'];
}
