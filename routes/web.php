<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('tracers', 'TracerController');
Route::resource('kuesioners', 'KuesionerController');
Route::post('regencies', 'KuesionerController@regencies')->name('regencies');
Route::group(['middleware' => 'auth'], function () {
    Route::resource('users', 'UserController');
    Route::put('/change_password/{id}', 'UserController@changePassword')->name('change_password');
    Route::get('/tracers-export', 'TracerController@export');
});

Auth::routes();
